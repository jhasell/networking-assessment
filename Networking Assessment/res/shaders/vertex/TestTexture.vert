#version 410

in vec4 Position;
in vec2 TexCoord;

out vec2 vTexCoord;

uniform mat4 ProjectionView;

void main()
{
	vTexCoord = TexCoord;
	gl_Position= ProjectionView * Position;
}