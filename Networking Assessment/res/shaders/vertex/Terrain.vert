#version 410

layout(location=0) in vec3 position;
layout(location=1) in vec3 normal;
layout(location=2) in vec2 texcoord;

out vec3 frag_position;
out vec3 frag_normal;
out vec2 frag_texcoord;

uniform mat4 projView;

void main()
{
	frag_position = position;
	frag_normal = normal;
	frag_texcoord = texcoord;

	gl_Position = projView * vec4(frag_position,1);
}