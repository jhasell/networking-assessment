#version 410

in vec4 Position;
in vec4 Colour;

out vec4 vColour;

uniform mat4 ProjectionView;
uniform float time;
uniform float heightScale;

void main()
{
	vColour = Colour;
	vec4 P = Position;
	p.y += sin(time + Position.x) * heightScale; 
	gl_Position = ProjectionView * P;
}