#version 410

layout(location=0) in vec4 position;
layout(location=1) in vec4 normal;
layout(location=2) in vec2 texcoord;

out vec4 frag_position;
out vec4 frag_normal;
out vec2 frag_texcoord;

uniform mat4 projView;

void main()
{
	frag_position = position;
	frag_normal = normal;
	frag_texcoord = vec2(-texcoord.x, -texcoord.y);

	gl_Position = projView * position;
}