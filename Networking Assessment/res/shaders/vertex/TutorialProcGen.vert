#version 410

layout(location=0) in vec4 position;
layout(location=1) in vec2 texcoord;

out vec2 frag_texcoord;

uniform mat4 projectionView;
uniform sampler2D perlin_texture;
uniform int useShaderPerlinFunction;
uniform float time;

float rows = 20;
float cols = 20;

int[256] permutation = int[256]( 151,160,137,91,90,15,
    131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,142,8,99,37,240,21,10,23,
    190, 6,148,247,120,234,75,0,26,197,62,94,252,219,203,117,35,11,32,57,177,33,
    88,237,149,56,87,174,20,125,136,171,168, 68,175,74,165,71,134,139,48,27,166,
    77,146,158,231,83,111,229,122,60,211,133,230,220,105,92,41,55,46,245,40,244,
    102,143,54, 65,25,63,161, 1,216,80,73,209,76,132,187,208, 89,18,169,200,196,
    135,130,116,188,159,86,164,100,109,198,173,186, 3,64,52,217,226,250,124,123,
    5,202,38,147,118,126,255,82,85,212,207,206,59,227,47,16,58,17,182,189,28,42,
    223,183,170,213,119,248,152, 2,44,154,163, 70,221,153,101,155,167, 43,172,9,
    129,22,39,253, 19,98,108,110,79,113,224,232,178,185, 112,104,218,246,97,228,
    251,34,242,193,238,210,144,12,191,179,162,241, 81,51,145,235,249,14,239,107,
    49,192,214, 31,181,199,106,157,184, 84,204,176,115,121,50,45,127, 4,150,254,
    138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,156,180 );

vec4 permute(vec4 x){return mod(((x*34.0)+1.0)*x, 289.0);}

float lerp(float a, float b, float x) { return a + x * (b - a); }

float fadeFloat(float t) { return t * t * t * (t * (t * 6 - 15) + 10); }

vec2 fadeVec2(vec2 t) { return t * t * t * (t * (t * 6 - 15) + 10); }

int inc(int num) { return num++; }

float grad(int hash, float x, float y, float z)
{
	int h = hash & 15;
	float u;
	float v;

	if(h < 8)
		u = x;
	else
		u = y;

	if(h < 4)
		v = y;
	else if(h == 12 || h == 14)
		v = x;
	else
		v = z;

	float result1;
	float result2;

	if((h&1) == 0)
		result1 = u;
	else
		result1 = -u;

	if((h&2) == 0)
		result2 = v;
	else
		result2 = -v;

	return result1 + result2;
}

float perlin(vec3 pos)
{
	int p[512] = int[512](0);
	for(int i = 0; i < 512; i++)
	{
		int index = i;
		if(index >= 512)
			index -= 512;

		p[index] = permutation[index];
	}

	//
	int xi = int(pos.x) & 255;
	int yi = int(pos.y) & 255;
	int zi = int(pos.z) & 255;
	float xf = pos.x-int(pos.x);
	float yf = pos.y-int(pos.y);
	float zf = pos.z-int(pos.z);

	// 
	float u = fadeFloat(xf);
	float v = fadeFloat(yf);
	float w = fadeFloat(zf);

	// Hash
	int aaa, aba, aab, abb, baa, bba, bab, bbb;
    aaa = p[p[p[    xi ]+    yi ]+    zi ];
    aba = p[p[p[    xi ]+inc(yi)]+    zi ];
    aab = p[p[p[    xi ]+    yi ]+inc(zi)];
    abb = p[p[p[    xi ]+inc(yi)]+inc(zi)];
    baa = p[p[p[inc(xi)]+    yi ]+    zi ];
    bba = p[p[p[inc(xi)]+inc(yi)]+    zi ];
    bab = p[p[p[inc(xi)]+    yi ]+inc(zi)];
    bbb = p[p[p[inc(xi)]+inc(yi)]+inc(zi)];

	// 
	float x1, x2, y1, y2;
	x1 = lerp(grad(aaa, xf, yf, zf), grad(baa, xf-1, yf, zf), u);
	x2 = lerp(grad(aba, xf, yf-1, zf), grad(bba, xf-1, yf-1, zf), u);
	y1 = lerp(x1, x2, v);

	x1 = lerp(grad(aab, xf, yf, zf-1), grad(bab, xf-1, yf, zf-1), u);
	x2 = lerp(grad(abb, xf, yf-1, zf-1), grad(bbb, xf-1, yf-1, zf-1), u);
	y2 = lerp(x1, x2, v);

	return (lerp(y1, y2, w) + 1) / 2;
}




float cnoise(vec2 P){
	vec4 Pi = floor(P.xyxy) + vec4(0.0, 0.0, 1.0, 1.0);
	vec4 Pf = fract(P.xyxy) - vec4(0.0, 0.0, 1.0, 1.0);
	
	Pi = mod(Pi, 289.0); // To avoid truncation effects in permutation
	
	vec4 ix = Pi.xzxz;
	vec4 iy = Pi.yyww;

	vec4 fx = Pf.xzxz;
	vec4 fy = Pf.yyww;

	vec4 i = permute(permute(ix) + iy);

	vec4 gx = 2.0 * fract(i * 0.0243902439) - 1.0; // 1/41 = 0.024...
	vec4 gy = abs(gx) - 0.5;
	vec4 tx = floor(gx + 0.5);
	gx = gx - tx;

	vec2 g00 = vec2(gx.x,gy.x);
	vec2 g10 = vec2(gx.y,gy.y);
	vec2 g01 = vec2(gx.z,gy.z);
	vec2 g11 = vec2(gx.w,gy.w);
	vec4 norm = 1.79284291400159 - 0.85373472095314 * 
		vec4(dot(g00, g00), dot(g01, g01), dot(g10, g10), dot(g11, g11));

	g00 *= norm.x;
	g01 *= norm.y;
	g10 *= norm.z;
	g11 *= norm.w;

	float n00 = dot(g00, vec2(fx.x, fy.x));
	float n10 = dot(g10, vec2(fx.y, fy.y));
	float n01 = dot(g01, vec2(fx.z, fy.z));
	float n11 = dot(g11, vec2(fx.w, fy.w));
	
	vec2 fade_xy = fadeVec2(Pf.xy);
	
	vec2 n_x = mix(vec2(n00, n01), vec2(n10, n11), fade_xy.x);
	float n_xy = mix(n_x.x, n_x.y, fade_xy.y);
	
	return 2.3 * n_xy;
}




float GenPerlinNoise()
{
	//if(useShaderPerlinFunction == 0)
		return texture(perlin_texture, texcoord).r * 5;
	
	float perlinVal = 0;
	
	float octaves = 6;
	float scale = (1.0 / (rows - 1)) * 3;
	float amplitude = 10.0;
	float persistance = 0.3;

	for(int i = 0; i < octaves; i++)
	{
		float freq = pow(2, float(i));
		float perlinSample = 1.0f;

		if(useShaderPerlinFunction == 0)
			perlinSample = perlin(position.xyz * scale * freq) * 0.5 + 0.5;
		else
			perlinSample = cnoise(position.xz * scale * freq) * 0.5 + 0.5;
		

		perlinVal += perlinSample * amplitude;
		amplitude *= persistance;
	}

	return perlinVal;
}

void main()
{
	frag_texcoord = vec2(1 - texcoord.x, texcoord.y);

	vec4 pos = position;
	pos.y += GenPerlinNoise();

	gl_Position = projectionView * pos;
}