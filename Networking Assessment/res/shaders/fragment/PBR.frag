#version 410

in vec4 frag_position;
in vec4 frag_normal;
in vec2 frag_texcoord;

out vec4 out_colour;

uniform sampler2D tex;

uniform vec4 cameraPosition;
uniform vec4 lightDir;
uniform vec4 lightColour;
uniform float roughness;
uniform float FresnelScale;
uniform vec4 ambientLight;
uniform float ambientLightMultiplier;

float GetDiffuse(vec4 E)
{
	// Oren-Nayar Diffuse Reflectance

	// N = surface normal
	// E = vector from surface to viewer
	// L is vector the light is coming from (surface to light)
	vec4 N = frag_normal;
	vec4 L = lightDir;
	float NdL = max(0.0f, dot(N, L));
	float NdE = max(0.0f, dot(N, E));

	float R2 = roughness * roughness;
	
	// Oren-Nayar Diffuse Term
	float A = 1.0f - 0.5f * R2 / (R2 + 0.33f);
	float B = 0.45f * R2 / (R2 + 0.09f);

	// CX = Max(0, cos(1,e))
	vec4 lightProjected = normalize(L - N * NdL);
	vec4 viewProjected = normalize(E - N * NdE);
	float CX = max(0.0f, dot(lightProjected, viewProjected));

	// DX = sin(alpha) * tan(beta)
	float alpha = sin(max(acos(NdE), acos(NdL)));
	float beta = tan(min(acos(NdE), acos(NdL)));
	float DX = alpha * beta;

	// Calculate Oren-Nayar, replaces the Phong Lambertian Term
	float OrenNayer = NdL * (A + B * CX * DX);
	
	return OrenNayer;
}

float GetSpecular(vec4 E)
{
	// Cook-Torrance Specular Reflectance
	vec4 N = frag_normal;
	vec4 L = lightDir;
	float e = 2.71828182845904523536028747135f;
	float pi = 3.1215926535897932384626433832f;
	float NdL = max(0.0f, dot(N, L));
	float NdE = max(0.0f, dot(N, E));

	float R2 = roughness * roughness;
	vec4 H = normalize(L + E);
	float NdH = max(0.0f, dot(N,H));
	float NdH2 = NdH * NdH;
	float HdE = max(0.0f, dot(H, E));

	// Beckman's Distrubution Function D
	float exponent = -(1-NdH2) / (NdH2 * R2);
	float D = pow(e, exponent) / (R2 * NdH2 * NdH2);

	// Fresnel Term F
	float F = mix(pow(1 - HdE, 5), 1, FresnelScale);

	// Geometric Attenuation Factor G
	float X = 2.0f * NdH / dot(E, H);
	float G = min(1, min(X * NdL, X * NdE));

	// Calculate Cook-Torrance
	float CookTorrance = max((D*G*F)/(NdE * pi), 0.0f);

	return CookTorrance;
}





void main()
{
	vec4 E = cameraPosition - frag_position;

	float diffuse = GetDiffuse(E);
	float spec = GetSpecular(E);

	out_colour = texture(tex, frag_texcoord);
	out_colour *= vec4(ambientLight * diffuse + lightColour * diffuse + lightColour * spec + ambientLight * ambientLightMultiplier);
}