#version 410

in vec4 vNormal;
in vec4 vPosition;

out vec4 FragColor;

uniform vec3 LightColour;
uniform vec3 LightDirection;

uniform vec3 AmbientLight;
uniform vec4 MaterialColour;

uniform vec3 CameraPos;
uniform float SpecPow;

void main()
{
	float d = max(0, dot( normalize(vNormal.xyz), LightDirection ) );

	vec3 E = normalize(CameraPos - vPosition.xyz);
	vec3 R = reflect(-LightDirection, vNormal.xyz);
	float s = max(0, dot(E,R));
	s = pow(s, SpecPow);

	FragColor = vec4(AmbientLight * d + LightColour * d + LightColour * s, 1);

	//FragColor = vec4(LightColour * d, 1);
}