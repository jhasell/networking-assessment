#version 410

in vec4 Colour;

out vec4 fragColour;
uniform sampler2D particleTexture;

void main()
{
	fragColour = Colour;
}