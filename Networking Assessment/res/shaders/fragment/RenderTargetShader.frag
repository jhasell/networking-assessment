#version 410

in vec4 frag_position;
in vec4 frag_normal;
in vec2 frag_texcoord;

out vec4 out_colour;

uniform sampler2D tex;

void main()
{
	out_colour = texture(tex, frag_texcoord);
	out_colour.a = 1;
}