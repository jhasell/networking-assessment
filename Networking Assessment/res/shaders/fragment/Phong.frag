#version 410

in vec4 frag_position;
in vec4 frag_normal;
in vec2 frag_texcoord;

out vec4 out_colour;

uniform sampler2D tex;
uniform vec4 cameraPosition;
uniform vec4 lightDir;
uniform vec4 lightColour;
uniform vec4 ambientLight;
uniform float specPower;

void main()
{
	float d = max(0, dot( normalize(frag_normal.xyz), lightDir.xyz ) );

	vec3 E = normalize(cameraPosition.xyz - frag_position.xyz);
	vec3 R = reflect(-lightDir.xyz, frag_normal.xyz);
	float s = max(0, dot(E,R));
	s = pow(s, specPower);

	out_colour = texture(tex, frag_texcoord);
	out_colour *= vec4(ambientLight * d + lightColour * d + lightColour * s);
}