#pragma once

#include "Engine\Resources\Shader.h"
#include "Engine\Camera\BaseCamera.h"

class TerrainShader : public Shader
{
public:
	TerrainShader(unsigned int grassTextureID, unsigned int rockTextureID);
	virtual ~TerrainShader();

	virtual void BindAttributes();
	virtual void GetAllUniformLocations();
	virtual void LoadTextures();
	virtual void Destroy() {};

	void LoadProjectionViewMatrix(const glm::mat4& projView);
	void LoadCameraPosition(const glm::vec3& pos);
	
	void LoadLightColour(const glm::vec4& colour);
	void LoadLightDirection(const glm::vec3& direction);
	void LoadTileSize(const glm::vec2& tileSize);
	void LoadMaxGrassHeight(float val);
	void LoadMinRockHeight(float val);
	
private:
	unsigned int m_grassTextureID;
	unsigned int m_rockTextureID;

	// All shader uniform locations
	int m_locationTileSize;
	int m_locationMaxGrassHeight;
	int m_locationMinRockHeight;
	int m_locationProjectionView;
	int m_locationCameraPos;
	int m_locationGrassTexture;
	int m_locationRockTexture;
	int m_locationLightColour;
	int m_locationLightDirection;
};