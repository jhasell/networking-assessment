#pragma once

#include "Engine\BaseApplication.h"
#include "Engine\Toolbox\Gizmos.h"

#include "..\..\BallBlasterServer\inc\NetworkMessages.h"

#include "Freetype\ft2build.h"
#include "Freetype\freetype\freetype.h"

#include "Engine\Camera\FlyCamera.h"
#include "Engine\Resources\Terrain.h"
#include "Engine\Resources\AssetLoader.h"
#include "Engine\Resources\Mesh.h"

#include "Engine\Physics\Collider\boundingSphere.h"
#include "Engine\Renderer\Renderer.h"

#include "Game\Shaders\TerrainShader.h"


class MyApplication : public BaseApplication
{
public:
	MyApplication(int width, int height, std::string title);
	virtual ~MyApplication();

	virtual bool Startup();
	virtual bool Shutdown();
	virtual void Update(float deltaTime);
	virtual void Render();

private:
	void HandleInput(float deltaTime);

	//-----------------------------//
	// Networking functions        //
	bool InitNetworkConnection();
	void HandleNetworkMessages();

	void HandleRemoteDisconnection();
	void HandleRemoteLostConnection();
	void HandleRemoteNewConnection();
	void HandleConnectRequestAccepted();
	void HandleNoFreeConnections();
	void HandleDisconnection();
	void HandleLostConnection();
	
	void HandleNewClientID(RakNet::BitStream& bs, RakNet::SystemAddress& ownerSysAddress);
	void HandleCreatePlayer(RakNet::BitStream& bs, RakNet::SystemAddress& ownerSysAddress);
	void HandleCreateBullet(RakNet::BitStream& bs, RakNet::SystemAddress& ownerSysAddress);
	void HandleUpdatePlayerPos(RakNet::BitStream& bs, RakNet::SystemAddress& ownerSysAddress);
	void HandleUpdatePlayerVel(RakNet::BitStream& bs, RakNet::SystemAddress& ownerSysAddress);

	void LogNetworkMessage(const char* message);
	void LogFailedStartup(RakNet::StartupResult startResult, RakNet::SocketDescriptor& sd);
	void LogFailedConnection(RakNet::ConnectionAttemptResult connectResult);
	// End of networking functions //
	//-----------------------------//

	const char* SERVER_IP = "127.0.0.1";
	const unsigned short PORT = 13579;
	RakNet::RakPeerInterface* m_peerInterface;

	unsigned int m_clientID;
	unsigned int m_clientGameObjectID;



	FlyCamera* m_camera;

	TerrainShader* m_goodTerrainShader;
	Terrain* m_terrain;

	InputManager* m_input;

	glm::vec4 m_clearColor;
};