#pragma once

#include "Engine\BaseApplication.h"

#include "..\..\BallBlasterServer\inc\NetworkMessages.h"

#include "Freetype\ft2build.h"
#include "Freetype\freetype\freetype.h"

#include "Engine\Debugging\DebugGUI.h"

#include <vector>


class LobbyApplication : public BaseApplication
{
public:
	LobbyApplication(int width, int height, std::string title);
	virtual ~LobbyApplication();

	virtual bool Startup();
	virtual bool Shutdown();
	virtual void Update(float deltaTime);
	virtual void Render();

	unsigned int GetClientID();
	RakNet::RakPeerInterface* GetPeerInterface();

private:
	void HandleInput(float deltaTime);

	bool InitNetworkConnection();
	void HandleNetworkMessages();

	void HandleRemoteDisconnection();
	void HandleRemoteLostConnection();
	void HandleRemoteNewConnection();
	void HandleConnectRequestAccepted();
	void HandleNoFreeConnections();
	void HandleDisconnection();
	void HandleLostConnection();

	void ReceivedClientID(RakNet::BitStream& bs, RakNet::SystemAddress& ownerSysAddress);
	void AddNewClient(RakNet::BitStream& bs, RakNet::SystemAddress& ownerSysAddress);
	void RemoveClient(RakNet::BitStream& bs, RakNet::SystemAddress& ownerSysAddress);
	void HandleRemoteClientReady(RakNet::BitStream& bs, RakNet::SystemAddress& ownerSysAddress);
	void HandleStartGame(RakNet::BitStream& bs, RakNet::SystemAddress& ownerSysAddress);

	void LogNetworkMessage(const char* message);
	void LogFailedStartup(RakNet::StartupResult startResult, RakNet::SocketDescriptor& sd);
	void LogFailedConnection(RakNet::ConnectionAttemptResult connectResult);


	DebugGUI* m_gui;
	const char* SERVER_IP = "127.0.0.1";
	const unsigned short PORT = 13579;
	RakNet::RakPeerInterface* m_peerInterface;

	unsigned int m_clientID;

	std::vector<LobbyData> lobbyClients;
};