#pragma once

#include "RendererData.h"
#include "..\Resources\Shader.h"

class Renderer
{
public:
	static void Prepare(const glm::vec4& clearColour);
	static void Render(RendererData* renData, Shader* shader);
	static void Render(RendererData* renData);

private:
	Renderer() = delete;
	Renderer(Renderer&) = delete;
	Renderer(Renderer&&) = delete;
	Renderer& operator=(Renderer&) = delete;
};