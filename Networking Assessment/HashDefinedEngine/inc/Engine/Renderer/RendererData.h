#pragma once

#include "AIE\gl_core_4_4.h"
#include <vector>

class RendererData
{
public:
	struct BufferData
	{
		GLuint bufferSize;
		GLvoid* buffer;
		GLenum drawType;

		BufferData(GLuint size, GLvoid* buf, GLenum type)
		{
			bufferSize = size;
			buffer = buf;
			drawType = type;
		}
	};

	struct AttributeData
	{
		GLuint m_index;
		GLint m_size;
		GLenum m_type;
		GLboolean m_isNormalized;
		GLsizei m_stride;
		GLvoid* m_ptr;

		AttributeData()
		{
			m_index = -1;
			m_size = -1;
			m_type = -1;
			m_isNormalized = GL_FALSE;
			m_stride = -1;
			m_ptr = 0;
		}

		AttributeData(GLuint index, GLint size, GLenum type, GLboolean norm, GLsizei stride, GLvoid* ptr)
		{
			m_index = index;
			m_size = size;
			m_type = type;
			m_isNormalized = norm;
			m_stride = stride;
			m_ptr = ptr;
		}
	};

	RendererData();
	virtual ~RendererData();
	
	void Init(BufferData& vbo, BufferData& ibo, 
		std::vector<AttributeData>& attribArray, unsigned int indexCount);

	//void Render(GLuint shaderID);
	
	void Destroy();

	GLuint GetVAO();
	GLuint GetIndexCount();

private:
	bool m_hasBeenDestroyed;
	GLuint m_vbo, m_ibo, m_vao;
	GLuint m_indexCount;



};