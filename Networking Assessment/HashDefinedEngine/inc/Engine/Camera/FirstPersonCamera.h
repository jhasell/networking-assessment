#pragma once

#include "BaseCamera.h"

class FirstPersonCamera : public BaseCamera
{
public:
	FirstPersonCamera();

	virtual void Update(float dt);

	void SetPos(const glm::vec3& pos);

private:
	FirstPersonCamera(FirstPersonCamera&) = delete;
	FirstPersonCamera(FirstPersonCamera&&) = delete;
	FirstPersonCamera& operator=(FirstPersonCamera&) = delete;


};