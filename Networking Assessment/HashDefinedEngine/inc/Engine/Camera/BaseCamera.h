#pragma once

#include "glm\glm.hpp"
#include "glm\ext.hpp"
#include "glm\gtc\quaternion.hpp"
#include "glm\gtx\quaternion.hpp"

class BaseCamera
{
public:
	enum EMoveType
	{
		UP,
		DOWN,
		LEFT,
		RIGHT,
		FORWARD,
		BACKWARD,
		PITCH_UP,
		PITCH_DOWN,
		YAW_LEFT,
		YAW_RIGHT,
		ROLL_LEFT,
		ROLL_RIGHT
	};

	BaseCamera();
	virtual ~BaseCamera();

	virtual void Update(float dt) = 0;

	void SetPerspective(float fov, float aspect, float near, float far);
	void SetLookAt(glm::vec3 from, glm::vec3 to);
	void SetPosition(glm::vec3 pos);
	void SetView(glm::mat4& view);
	void SetWorldTransform(glm::mat4& world);

	glm::vec3 GetPosition();
	glm::mat4& GetWorldTransform();
	glm::mat4& GetViewTransform();
	glm::mat4& GetProjectionTransform();
	glm::mat4& GetProjectionViewTransform();
	glm::vec4* GetFrustrumPlanes();

private:
	void UpdateProjectionViewTransform();

	glm::mat4 m_viewTransform;
	glm::mat4 m_worldTransform;
	glm::mat4 m_projectionTransform;
	glm::mat4 m_projectionViewTransform;
};