#pragma once

#include "BaseCamera.h"
#include "GLFW\glfw3.h"

class FlyCamera : public BaseCamera
{
public:
	FlyCamera();
	virtual void Update(float dt);

	void Move(EMoveType movement, float dt);

	void SetMoveSpeed(float speed);
	void SetRotateSpeed(float speed);

private:
	void HandleKeyboardInput(GLFWwindow* window, float dt);
	void HandleMouseInput(GLFWwindow* window, float dt);
	void CalculateRotation(float dt, float xOffset, float yOffset);

	float m_moveSpeed;
	float m_rotateSpeed;
};