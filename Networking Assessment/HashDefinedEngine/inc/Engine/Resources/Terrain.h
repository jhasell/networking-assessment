#pragma once

#include "Mesh.h"
#include "Shader.h"

class Terrain
{
public:
	Terrain(Shader* shader, unsigned int rows, unsigned int cols, float spacing);
	virtual ~Terrain();

	//void Render();

	unsigned int GetNumRows();
	unsigned int GetNumCols();

	Mesh* GetMesh();
	Shader* GetShader();

private:
	bool Generate(unsigned int rows, unsigned int cols, float spacing = 1.0f);
	float* GeneratePerlin(float scale, unsigned int octaves, float amplitude, float persistance);

	Mesh* m_mesh;
	Shader* m_shader;

	unsigned int m_rows;
	unsigned int m_cols;
};