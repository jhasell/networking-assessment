#pragma once

#include <iostream>
#include <fstream>
#include <string>

#include <Windows.h> // Used to get current working directory

#include "Mesh.h"
#include "FBXModel.h"
//#include "Shader.h"
//#include "Texture.h"
//#include "GameObject.h"


class AssetLoader
{
public:
	static std::string GetResourceFolder();

	static std::string ReadFile(const char* file);
	static std::string ReadFile(std::string file);

	static unsigned int LoadShader(unsigned int type, const char* filename);
	static unsigned int LoadTexture(const char* filename);

	//static Shader* LoadShaderFromFile(const char* vertexFilename, const char* fragmentFilename);
	//static Texture* LoadTextureFromFile(const char* path, bool isFullePath = false);
	static FBXModel* LoadFBXObjFromFile(const char* fbxFile);
	static Mesh* LoadObjFromFile(const char* objFile);
	static Mesh* GenerateGrid(unsigned int rows, unsigned int cols, float spacing = 1.0f);
	static Mesh* GenerateGrid(unsigned int rows, unsigned int cols, float* heightData, float spacing = 1.0f);
	static Mesh* GenerateSphere(glm::vec3& center, float radius, unsigned int rows, unsigned int cols, glm::vec4& fillColour = glm::vec4(1, 1, 1, 1));
	static Mesh* GenerateQuad();
	static Mesh* GenerateCube();

private:
	static const char* TEX_REL_PATH;
	static const char* VERT_REL_PATH;
	static const char* FRAG_REL_PATH;
	static const char* GEOM_REL_PATH;
	static const char* TESS_CONT_REL_PATH;
	static const char* TESS_EVAL_REL_PATH;

	struct Vertex
	{
		glm::vec3 position;
		glm::vec3 normal;
		glm::vec2 texcoord;
	};

	AssetLoader() {}
	AssetLoader(AssetLoader&) {}
};