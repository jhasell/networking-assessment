#pragma once

#include "Mesh.h"
#include "Shader.h"
#include "FBXModel.h"
#include "..\Physics\Collider\AABB.h"
#include "..\Physics\Collider\boundingSphere.h"

#include "glm\glm.hpp"
#include "glm\ext.hpp"

class GameObject
{
public:
	GameObject();
	GameObject(Mesh* mesh, Shader* shader);
	GameObject(FBXModel* fbx, Shader* shader);
	~GameObject();
	
	virtual void Update(float deltaTime);
	//virtual void Render();

	void Destroy();

	glm::mat4& GetMVP(const glm::mat4& projectionView, glm::mat4& mvp);

	void SetScale(glm::vec3 scale);
	void SetScale(float scale);
	void SetRotation(glm::vec3 rotation);
	void SetPosition(glm::vec3 position);
	void SetMesh(Mesh* mesh);
	void SetFBX(FBXModel* fbx);
	void SetShader(Shader* shader);

	glm::vec3 GetScale();
	glm::vec3 GetRotation();
	glm::vec3 GetPosition();
	Mesh* GetMesh();
	FBXModel* GetFBX();
	Shader* GetShader();
	ICollider* GetCollider();
	

private:
	void UpdateColliderPosition();

	bool m_hasBeenDestroyed;

	Mesh* m_mesh;
	FBXModel* m_fbx;

	Shader* m_shader;

	glm::vec3 m_scale;
	glm::vec3 m_rotation;
	glm::vec3 m_position;

	ICollider* m_collider;
};