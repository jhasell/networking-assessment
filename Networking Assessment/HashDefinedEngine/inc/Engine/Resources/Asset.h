#pragma once

#include "AIE\gl_core_4_4.h"

class Asset
{
public:
	enum AssetType
	{
		SHADER,
		TEXTURE,
		MESH,
		MODEL,
		FBX_MODEL,
		OBJ_MODEL
	};

	Asset(AssetType type) : m_type(type), m_hasBeenDestroyed(false) {}
	virtual ~Asset() {}

	AssetType GetType() { return m_type; }

	virtual void Destroy() = 0;
	
protected:
	bool m_hasBeenDestroyed;

private:
	AssetType m_type;
};