#pragma once

#include "..\Renderer\RendererData.h"

#include "Asset.h"
#include "Shader.h"

#include "FBXLoader\FBXFile.h"

#include "..\Physics\Collider\ICollider.h"


class FBXModel : public Asset
{
public:
	FBXModel(FBXFile* fbx, ICollider* collider);
	~FBXModel();

	void UpdateAnimation(float time, bool loop = true, float fps = 24.0f);

	//void Render(GLuint shaderID);

	void Destroy();

	FBXSkeleton* GetSkeleton(unsigned int index);
	FBXSkeleton* GetSkeleton() const;
	FBXAnimation* GetAnimation(unsigned int index);
	FBXAnimation* GetAnimation(const char* name);
	FBXAnimation* GetAnimation() const;

	ICollider* GetCollider();

	void SetSkeleton(FBXSkeleton* skeleton);
	void SetAnimation(FBXAnimation* animation);

	static const char* const FBX_RELATIVE_PATH;
	static const char* const OBJ_RELATIVE_PATH;

private:
	bool m_hasBeenDestroyed;
	bool m_hasAnimations;

	FBXFile* m_fbxFile;
	FBXSkeleton* m_skeleton;
	FBXAnimation* m_animation;

	ICollider* m_collider;
};