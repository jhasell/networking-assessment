#pragma once

#include "Asset.h"
#include <string>
#include "glm\glm.hpp"

class Shader : public Asset
{
public:
	Shader(const char* vsFile, const char* fsFile, const char* gsFile = nullptr, const char* tcsFile = nullptr, const char* tesFile = nullptr);
	virtual ~Shader();

	virtual void LoadTextures() {};

	void Start();
	void Stop();

	unsigned int GetProgramID();

protected:
	virtual void BindAttributes() = 0;
	virtual void GetAllUniformLocations() = 0;

	void CreateProgram();
	void BindAttribute(int attrLocation, const char* name);
	int GetUniformLocation(const char* uniformName);

	void LoadFloat(int location, float value);
	void LoadVec2(int location, const glm::vec2& value);
	void LoadVec3(int location, const glm::vec3& value);
	void LoadVec4(int location, const glm::vec4& value);
	void LoadBoolean(int location, bool value);
	void LoadMat4(int location, const glm::mat4& value);
	void LoadSampler2D(int uniformLocation, unsigned int textureSlot, unsigned int textureID);
	
private:
	unsigned int m_programID;

	unsigned int m_vertID;
	unsigned int m_fragID;
	unsigned int m_geomID;
	unsigned int m_tessContID;
	unsigned int m_tessEvalID;
};