#pragma once

#include "Asset.h"
#include "..\Renderer\RendererData.h"
#include "..\Physics\Collider\ICollider.h"

class Mesh : public Asset
{
public:
	Mesh(RendererData* renData, ICollider* collider);
	virtual ~Mesh();

	//void Render(GLuint shaderID);

	void Destroy();

	ICollider* GetCollider();
	RendererData* GetRenderData();
	
private:
	RendererData* m_rendererData;
	ICollider* m_collider;
};