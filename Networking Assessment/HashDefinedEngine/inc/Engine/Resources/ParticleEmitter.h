#pragma once

#include "glm\glm.hpp"
#include "glm\ext.hpp"
#include "AIE\gl_core_4_4.h"


class ParticleEmitter // Point emitter
{
public:
	enum ParticleType
	{
		POINT,
		SPHERE,
		RECTANGLE,
		PLANE,
		LINE,
		SPHERE_SURFACE,
		RING
	};

	struct Particle
	{
		glm::vec3 position;
		glm::vec3 velocity;
		glm::vec4 colour;
		float size;
		float lifetime;
		float lifespan;
	};

	struct ParticleVertex
	{
		glm::vec4 position;
		glm::vec4 colour;
	};


	ParticleEmitter(ParticleType type);
	virtual ~ParticleEmitter();

	void Initialise(unsigned int maxParticles, unsigned int emitRate, float lifetimeMin,
		float lifetimeMax, float velocityMin, float velocityMax, float startSize, float endSize,
		const glm::vec4& startColour, const glm::vec4& endColour);

	void Update(float deltaTime, const glm::mat4& cameraTransform);
	void Draw();

protected:
	Particle* m_particles;
	unsigned int m_firstDead;
	unsigned int m_maxParticles;

	unsigned int m_vao, m_vbo, m_ibo;
	ParticleVertex* m_vertexData;

private:
	void Emit();
	void EmitPoint(Particle& particle);
	void EmitSphere(Particle& particle);
	void EmitRectangle(Particle& particle);
	void EmitPlane(Particle& particle);
	void EmitLine(Particle& particle);
	void EmitSphereSurface(Particle& particle);
	void EmitRing(Particle& particle);




	ParticleType m_type;

	glm::vec3 m_position;

	float m_emitTimer;
	float m_emitRate;

	float m_lifeSpanMin;
	float m_lifeSpanMax;

	float m_velocityMin;
	float m_velocityMax;

	float m_startsize;
	float m_endsize;

	glm::vec4 m_startColour;
	glm::vec4 m_endColour;
};