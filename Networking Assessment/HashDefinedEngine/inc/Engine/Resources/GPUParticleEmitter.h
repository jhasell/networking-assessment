#pragma once

#include "glm\glm.hpp"
#include "AIE\gl_core_4_4.h"

struct GPUParticle
{
	GPUParticle() : lifetime(1), lifespan(0) {}

	glm::vec3 position;
	glm::vec3 velocity;
	float lifetime;
	float lifespan;
};

class GPUParticleEmitter
{
public:
	GPUParticleEmitter();
	virtual ~GPUParticleEmitter();

	void Init(unsigned int maxParticles,
		float lifespanMin, float lifespanMax,
		float velMin, float velMax,
		float startSize, float endSize,
		const glm::vec4& startColour,
		const glm::vec4& endColour);

	void Render(float time, const glm::mat4& camera, const glm::mat4& projView);
	void SetPosition(const glm::vec3& pos);

protected:
	void CreateBuffers();
	void CreateUpdateShader();
	void CreateDrawShader();

	GPUParticle* m_particles;

	unsigned int m_maxParticles;

	glm::vec3 m_position;

	float m_lifespanMin;
	float m_lifespanMax;

	float m_velocityMin;
	float m_velocityMax;

	float m_startSize;
	float m_endSize;

	glm::vec4 m_startColour;
	glm::vec4 m_endColour;

	unsigned int m_activeBuffer;
	unsigned int m_vao[2];
	unsigned int m_vbo[2];

	unsigned int m_drawShader;
	unsigned int m_updateShader;

	float m_lastDrawTime;

private:
	unsigned int LoadShader(unsigned int type, const char* path);
};