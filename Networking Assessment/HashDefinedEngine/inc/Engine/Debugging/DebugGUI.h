#pragma once

#include "..\BaseApplication.h"
#include "AntTweakBar\AntTweakBar.h"

class DebugGUI
{
public:
	DebugGUI(const BaseApplication* app);
	virtual ~DebugGUI();

	void AddRW(const char* name, TwType type, void* var, const char* properties = "");
	void AddRW(TwBar* bar, const char* name, TwType type, void* var, const char* properties = "");
	void AddSeparator(const char* name, TwBar* bar = nullptr, const char* properties = "");

	static TwBar* CreateBar(const char* name);
	TwBar* GetDefaultBar();

	void Render();
	void Destroy();

private:
	TwBar* m_defaultBar;
	bool m_hasBeenDestroyed;
};