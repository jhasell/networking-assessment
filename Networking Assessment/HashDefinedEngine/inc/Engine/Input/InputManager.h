#pragma once
#include "glm\glm.hpp"

class BaseApplication;

class InputManager
{
public:
	InputManager(BaseApplication* app);

	bool IsKeyDown(int key);
	bool IsMouseButtonDown(int btn);
	glm::vec2 GetMousePos();

private:
	InputManager(InputManager&) = delete;
	InputManager& operator=(InputManager&) = delete;

	BaseApplication* m_app;
};