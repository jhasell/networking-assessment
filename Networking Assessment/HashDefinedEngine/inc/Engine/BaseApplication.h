#pragma once

#include <AIE\gl_core_4_4.h>
#include <GLFW\glfw3.h>
#include <string>
#include "Input\InputManager.h"

class BaseApplication
{
public:
	BaseApplication(int width, int height, std::string title);
	virtual ~BaseApplication();

	bool Run();

	GLFWwindow* GetWindow() const;
	int GetWindowWidth() const;
	int GetWindowHeight() const;
	InputManager* GetInputManager() const;
	
protected:
	virtual bool Startup() = 0;
	virtual bool Shutdown() = 0;
	virtual void Update(float dt) = 0;
	virtual void Render() = 0;

	void Stop();

	int GetFPS();
	void ShowWireframe(bool show);
	bool IsWireframeShowing();

	GLFWwindow* m_window;
	std::string m_windowTitle;
	int m_windowWidth;
	int m_windowHeight;

private:
	BaseApplication(const BaseApplication&) = delete;
	BaseApplication(BaseApplication&&) = delete;
	const void operator=(BaseApplication&) = delete;
	
	bool InitGLFW();
	void DeinitGLFW();
	
	void CenterGLFWWindow(GLFWwindow* window);

	bool m_isGLFWInitialised;
	bool m_isRunning;
	int m_fps;
	bool m_showWireframe;
	InputManager* m_inputManager;

};