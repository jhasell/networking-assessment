#pragma once

#include "ICollider.h"
#include <vector>
#include "glm\glm.hpp"

class BoundingSphere : public ICollider
{
public:
	BoundingSphere();
	virtual ~BoundingSphere();

	void Fit(const std::vector<glm::vec3>& points);

	static bool HasCollidedWithPlane(const glm::vec3 sphereCenter, float sphereRadius, glm::vec4& plane);


	glm::vec3 m_center;
	float m_radius;
};