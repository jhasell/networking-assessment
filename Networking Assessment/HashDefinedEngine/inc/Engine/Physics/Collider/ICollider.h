#pragma once

class ICollider
{
public:
	enum ColliderType
	{
		AABB,
		SPHERE,
		PLANE,
		NONE
	};

	ICollider(ColliderType type) : m_type(type) {}
	virtual ~ICollider() {}

	ColliderType GetType() { return m_type; }

private:
	ColliderType m_type;
};