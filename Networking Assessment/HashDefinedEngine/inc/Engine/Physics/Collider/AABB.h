#pragma once

#include "ICollider.h"
#include <vector>
#include "glm\glm.hpp"

class AABB : public ICollider
{
public:
	AABB();
	virtual ~AABB();

	void Reset();
	void Fit(const std::vector<glm::vec3>& points);

	static bool HasCollidedWithPlane(const glm::vec3 min, const glm::vec3 max, glm::vec4& plane);


	glm::vec3 m_min, m_max;
};