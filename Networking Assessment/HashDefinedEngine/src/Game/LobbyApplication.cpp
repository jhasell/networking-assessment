#include "Game\LobbyApplication.h"

#include <iostream>

LobbyApplication::LobbyApplication(int width, int height, std::string title)
	: BaseApplication(width, height, title)
{
	m_clientID = 0;
	m_gui = nullptr;
}

LobbyApplication::~LobbyApplication()
{

}

bool LobbyApplication::Startup()
{
	bool result = InitNetworkConnection();
	if (!result)
		return false;

	m_gui = new DebugGUI(this);
	m_gui->AddRW("Client ID: ", TwType::TW_TYPE_UINT32, &m_clientID);
	
	return true;
}

bool LobbyApplication::Shutdown()
{
	return true;
}

void LobbyApplication::Update(float deltaTime)
{
	if (glfwWindowShouldClose(GetWindow()))
	{
		this->Stop();
		return;
	}

	HandleNetworkMessages();
	HandleInput(deltaTime);
}

void LobbyApplication::Render()
{
	if(m_gui != nullptr)
		TwDraw();
}

unsigned int LobbyApplication::GetClientID()
{
	return m_clientID;
}

RakNet::RakPeerInterface* LobbyApplication::GetPeerInterface()
{
	return m_peerInterface;
}

void LobbyApplication::HandleInput(float deltaTime)
{
	if (glfwGetKey(m_window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
	{
		this->Stop();
		return;
	}
}

bool LobbyApplication::InitNetworkConnection()
{
	bool result = false;

	LogNetworkMessage("Attempting to start client network interface.");
	m_peerInterface = RakNet::RakPeerInterface::GetInstance();
	RakNet::SocketDescriptor sd;
	RakNet::StartupResult startResult = m_peerInterface->Startup(1, &sd, 1);

	if (startResult == RakNet::StartupResult::RAKNET_STARTED)
	{
		LogNetworkMessage("Client network interface started successfully.");
		LogNetworkMessage("Attempting to connect to server.");
		RakNet::ConnectionAttemptResult connectResult = m_peerInterface->Connect(SERVER_IP, PORT, nullptr, 0);

		if (connectResult == RakNet::ConnectionAttemptResult::CONNECTION_ATTEMPT_STARTED)
			result = true;
		else
			LogFailedConnection(connectResult);
	}
	else
	{
		LogFailedStartup(startResult, sd);
	}

	return result;
}

void LobbyApplication::HandleNetworkMessages()
{
	RakNet::Packet* packet;

	for (packet = m_peerInterface->Receive(); packet;
	m_peerInterface->DeallocatePacket(packet),
		packet = m_peerInterface->Receive())
	{
		switch (packet->data[0])
		{
			case ID_REMOTE_DISCONNECTION_NOTIFICATION:
			{
				HandleRemoteDisconnection();
				break;
			}
			case ID_REMOTE_CONNECTION_LOST:
			{
				HandleRemoteLostConnection();
				break;
			}
			case ID_REMOTE_NEW_INCOMING_CONNECTION:
			{
				HandleRemoteNewConnection();
				break;
			}
			case ID_CONNECTION_REQUEST_ACCEPTED:
			{
				HandleConnectRequestAccepted();
				break;
			}
			case ID_NO_FREE_INCOMING_CONNECTIONS:
			{
				HandleNoFreeConnections();
				break;
			}
			case ID_DISCONNECTION_NOTIFICATION:
			{
				HandleDisconnection();
				break;
			}
			case ID_CONNECTION_LOST:
			{
				HandleLostConnection();
				break;
			}
			case ID_SERVER_LOBBY_CLIENT_ID:
			{
				RakNet::BitStream bs(packet->data, packet->length, false);
				bs.IgnoreBytes(sizeof(RakNet::MessageID));
				ReceivedClientID(bs, packet->systemAddress);
				break;
			}
			case ID_SERVER_NEW_CLIENT:
			{
				RakNet::BitStream bs(packet->data, packet->length, false);
				bs.IgnoreBytes(sizeof(RakNet::MessageID));
				AddNewClient(bs, packet->systemAddress);
				break;
			}
			case ID_SERVER_REMOVE_CLIENT:
			{
				RakNet::BitStream bs(packet->data, packet->length, false);
				bs.IgnoreBytes(sizeof(RakNet::MessageID));
				RemoveClient(bs, packet->systemAddress);
				break;
			}
			case ID_SERVER_LOBBY_CLIENT_READY:
			{
				RakNet::BitStream bs(packet->data, packet->length, false);
				bs.IgnoreBytes(sizeof(RakNet::MessageID));
				HandleRemoteClientReady(bs, packet->systemAddress);
				break;
			}
			case ID_SERVER_START_GAME:
			{
				RakNet::BitStream bs(packet->data, packet->length, false);
				bs.IgnoreBytes(sizeof(RakNet::MessageID));
				HandleStartGame(bs, packet->systemAddress);
				break;
			}
			default:
			{
				LogNetworkMessage("Received unknown id: " + packet->data[0]);
				break;
			}
		}
	}
}

void LobbyApplication::HandleRemoteDisconnection()
{
	LogNetworkMessage("Another client has disconnected.");
}

void LobbyApplication::HandleRemoteLostConnection()
{
	LogNetworkMessage("Another client has lost connection.");
}

void LobbyApplication::HandleRemoteNewConnection()
{
	LogNetworkMessage("Another client has connected.");
}

void LobbyApplication::HandleConnectRequestAccepted()
{
	LogNetworkMessage("Our connection request has been accepted.");
}

void LobbyApplication::HandleNoFreeConnections()
{
	LogNetworkMessage("The server is full.");
}

void LobbyApplication::HandleDisconnection()
{
	LogNetworkMessage("We have been disconnected.");
}

void LobbyApplication::HandleLostConnection()
{
	LogNetworkMessage("Connection lost.");
}

void LobbyApplication::ReceivedClientID(RakNet::BitStream& bs, RakNet::SystemAddress& ownerSysAddress)
{
	bs.Read(m_clientID);

	lobbyClients.push_back(LobbyData(m_clientID));

	LogNetworkMessage("Received clientID: " + m_clientID);
}

void LobbyApplication::AddNewClient(RakNet::BitStream& bs, RakNet::SystemAddress& ownerSysAddress)
{
	unsigned int newClientID;

	bs.Read(newClientID);

	bool clientExist = false;
	for (auto client : lobbyClients)
	{
		if (client.clientID == newClientID)
		{
			clientExist = true;
			LogNetworkMessage("Received new client data, but it already exists! Client ID: " + newClientID);
			break;
		}
	}

	if (!clientExist)
	{
		lobbyClients.push_back(LobbyData(newClientID));
		LogNetworkMessage("Received new client data! Client ID: " + newClientID);
	}
}

void LobbyApplication::RemoveClient(RakNet::BitStream& bs, RakNet::SystemAddress& ownerSysAddress)
{
	unsigned int id;

	bs.Read(id);

	for (auto i = lobbyClients.begin(); i != lobbyClients.end(); i++)
	{
		if (i->clientID == id)
		{
			lobbyClients.erase(i);
			LogNetworkMessage("Removed client, ID: " + id);
			break;
		}
	}
}

void LobbyApplication::HandleRemoteClientReady(RakNet::BitStream& bs, RakNet::SystemAddress& ownerSysAddress)
{
	unsigned int id, mapNum;
	bs.Read(id);
	bs.Read(mapNum);

	for (auto client : lobbyClients)
	{
		if(client.clientID == id)
		{
			client.isReady = true;
			client.mapRequestedNum = mapNum;
			LogNetworkMessage("A Client is ready! Client ID: " + id);
		}
	}
}

void LobbyApplication::HandleStartGame(RakNet::BitStream& bs, RakNet::SystemAddress& ownerSysAddress)
{
	this->Stop();
}

void LobbyApplication::LogNetworkMessage(const char* message)
{
	std::cout << message << std::endl;
}

void LobbyApplication::LogFailedStartup(RakNet::StartupResult startResult, RakNet::SocketDescriptor& sd)
{
	LogNetworkMessage("Failed to start server.");
	std::string errorTag;

	switch (startResult)
	{
	case RakNet::StartupResult::COULD_NOT_GENERATE_GUID:
		errorTag = "COULD_NOT_GENERATE_GUID"; break;
	case RakNet::StartupResult::FAILED_TO_CREATE_NETWORK_THREAD:
		errorTag = "FAILED_TO_CREATE_NETWORK_THREAD"; break;
	case RakNet::StartupResult::INVALID_MAX_CONNECTIONS:
		errorTag = "INVALID_MAX_CONNECTIONS"; break;
	case RakNet::StartupResult::INVALID_SOCKET_DESCRIPTORS:
		errorTag = "INVALID_SOCKET_DESCRIPTORS"; break;
	case RakNet::StartupResult::PORT_CANNOT_BE_ZERO:
		errorTag = "PORT_CANNOT_BE_ZERO"; break;
	case RakNet::StartupResult::RAKNET_ALREADY_STARTED:
		errorTag = "RAKNET_ALREADY_STARTED"; break;
	case RakNet::StartupResult::SOCKET_FAILED_TEST_SEND:
		errorTag = "SOCKET_FAILED_TEST_SEND"; break;
	case RakNet::StartupResult::SOCKET_FAILED_TO_BIND:
		errorTag = "SOCKET_FAILED_TO_BIND"; break;
	case RakNet::StartupResult::SOCKET_FAMILY_NOT_SUPPORTED:
		errorTag = "SOCKET_FAMILY_NOT_SUPPORTED"; break;
	case RakNet::StartupResult::SOCKET_PORT_ALREADY_IN_USE:
		errorTag = "SOCKET_PORT_ALREADY_IN_USE"; break;
	case RakNet::StartupResult::STARTUP_OTHER_FAILURE:
		errorTag = "STARTUP_OTHER_FAILURE"; break;
	default:
		errorTag = "UNKNOWN"; break;
	}

	std::string msg = "";
	msg += "--------------------------------------------------------" + '\n';
	msg += "StartupResult Error: " + errorTag + '\n';
	msg += "--------------------------------------------------------" + '\n';
	msg += "PORT: " + PORT + '\n';
	msg += "sd.blockingSocket: " + sd.blockingSocket + '\n';
	msg += "sd.chromeInstance: " + sd.chromeInstance + '\n';
	msg += "sd.extraSocketOptions: " + sd.extraSocketOptions + '\n';
	msg += "sd.hostAddress: " + std::string(sd.hostAddress) + '\n';
	msg += "sd.port: " + sd.port + '\n';
	msg += "sd.remotePortRakNetWasStartedOn_PS3_PSP2: " + sd.remotePortRakNetWasStartedOn_PS3_PSP2 + '\n';
	msg += "sd.socketFamily: " + sd.socketFamily + '\n';
	msg += "--------------------------------------------------------" + '\n';

	LogNetworkMessage(msg.c_str());
}

void LobbyApplication::LogFailedConnection(RakNet::ConnectionAttemptResult connectResult)
{
	std::string errorTag;

	switch (connectResult)
	{
	case RakNet::ConnectionAttemptResult::ALREADY_CONNECTED_TO_ENDPOINT:
		errorTag = "ALREADY_CONNECTED_TO_ENDPOINT"; break;
	case RakNet::ConnectionAttemptResult::CANNOT_RESOLVE_DOMAIN_NAME:
		errorTag = "CANNOT_RESOLVE_DOMAIN_NAME"; break;
	case RakNet::ConnectionAttemptResult::CONNECTION_ATTEMPT_ALREADY_IN_PROGRESS:
		errorTag = "CONNECTION_ATTEMPT_ALREADY_IN_PROGRESS"; break;
	case RakNet::ConnectionAttemptResult::INVALID_PARAMETER:
		errorTag = "INVALID_PARAMETER"; break;
	case RakNet::ConnectionAttemptResult::SECURITY_INITIALIZATION_FAILED:
		errorTag = "SECURITY_INITIALIZATION_FAILED"; break;
	default:
		errorTag = "UNKNOWN"; break;
	}

	std::string msg = "";
	msg += "--------------------------------------------------------" + '\n';
	msg += "Server IP: " + std::string(SERVER_IP) + '\n';
	msg += "Port: " + PORT + '\n';
	msg += "ConnectionResult tag: " + errorTag + '\n';
	msg += "--------------------------------------------------------" + '\n';

	LogNetworkMessage(msg.c_str());
}