#include "Game\MyApplication.h"


MyApplication::MyApplication(int width, int height, std::string title)
	: BaseApplication(width, height, title)
{
	m_camera = nullptr;
	m_goodTerrainShader = nullptr;
	m_input = nullptr;
	m_terrain = nullptr;
	m_clearColor = glm::vec4(0.2f, 0.4f, 1.0f, 1.0f);

	m_clientID = 0;
	m_clientGameObjectID = 0;
}

MyApplication::~MyApplication()
{
	Shutdown();
}

bool MyApplication::Startup()
{
	Gizmos::create();

	m_input = this->GetInputManager();

	// Create camera
	m_camera = new FlyCamera();
	m_camera->SetLookAt(glm::vec3(0, 200, 200), glm::vec3(0, 100, 0));
	float aspect = GetWindowWidth() / (float)GetWindowHeight();
	m_camera->SetPerspective(glm::pi<float>() * 0.25f, aspect, 0.1f, 1000.0f);
	m_camera->SetMoveSpeed(30.0f);

	// Load textures
	unsigned int grass = AssetLoader::LoadTexture("grass.jpg");
	unsigned int rock = AssetLoader::LoadTexture("rock.jpg");
	
	// Load shaders
	m_goodTerrainShader = new TerrainShader(grass, rock);

	// Load terrain
	m_terrain = new Terrain(m_goodTerrainShader, 64, 64, 5.0f);

	// Initialise shaders
	glm::vec2 tileSize(1.0f / m_terrain->GetNumRows(), 1.0f / m_terrain->GetNumCols());
	
	m_goodTerrainShader->Start();
	m_goodTerrainShader->LoadLightColour(glm::vec4(1, 1, 1, 1));
	m_goodTerrainShader->LoadLightDirection(glm::vec3(0, 1, 0));
	m_goodTerrainShader->LoadTileSize(tileSize);
	m_goodTerrainShader->LoadMaxGrassHeight(73);
	m_goodTerrainShader->LoadMinRockHeight(75);
	m_goodTerrainShader->Stop();
		
	return true;
}

bool MyApplication::Shutdown()
{
	if (m_camera != nullptr)
	{
		delete m_camera;
		m_camera = nullptr;
	}

	if (m_goodTerrainShader != nullptr)
	{
		delete m_goodTerrainShader;
		m_goodTerrainShader = nullptr;
	}

	if (m_terrain != nullptr)
	{
		delete m_terrain;
		m_terrain = nullptr;
	}

	return true;
}

void MyApplication::Update(float deltaTime)
{
	Gizmos::clear();

	m_camera->Update(deltaTime);

	Gizmos::addSphere(glm::vec3(0, 100, 0), 5, 20, 20, glm::vec4(1, 0, 0, 1));

	// Update shaders
	if (m_goodTerrainShader != nullptr)
	{
		m_goodTerrainShader->Start();
		m_goodTerrainShader->LoadProjectionViewMatrix(m_camera->GetProjectionViewTransform());
		m_goodTerrainShader->LoadCameraPosition(m_camera->GetPosition());
		m_goodTerrainShader->Stop();
	}
	
	HandleInput(deltaTime);

	// Check if window should close
	if (glfwWindowShouldClose(m_window))
		this->Stop();
}

void MyApplication::Render()
{
	Renderer::Prepare(m_clearColor);
	Gizmos::draw(m_camera->GetProjectionViewTransform());

	// Draw terrain
	if (m_terrain != nullptr && m_goodTerrainShader != nullptr)
	{
		Renderer::Render(m_terrain->GetMesh()->GetRenderData(), m_goodTerrainShader);
	}
}

void MyApplication::HandleInput(float deltaTime)
{
	if (glfwGetKey(m_window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
	{
		this->Stop();
		return;
	}

	// Create timers to ensure minimum time between button presses
	static const float btnPressDelay = 0.1f;
	static float wireframeTimer = btnPressDelay;
	static float shaderPerlinTimer = btnPressDelay;

	// Update button timers
	if (wireframeTimer > 0.0f)
		wireframeTimer -= deltaTime;
	if (shaderPerlinTimer > 0.0f)
		shaderPerlinTimer -= deltaTime;


	// Toggle wire frame
	if (m_input->IsKeyDown(GLFW_KEY_O))
	{
		if (wireframeTimer <= 0.0f)
		{
			wireframeTimer = btnPressDelay;
			this->ShowWireframe(!this->IsWireframeShowing());
		}
	}

	if (m_input->IsKeyDown(GLFW_KEY_LEFT_SHIFT))
		m_camera->SetMoveSpeed(80.0f);
	else
		m_camera->SetMoveSpeed(30.0f);
}

bool MyApplication::InitNetworkConnection()
{
	bool result = false;

	LogNetworkMessage("Attempting to start client network interface.");
	m_peerInterface = RakNet::RakPeerInterface::GetInstance();
	RakNet::SocketDescriptor sd;
	RakNet::StartupResult startResult = m_peerInterface->Startup(1, &sd, 1);

	if (startResult == RakNet::StartupResult::RAKNET_STARTED)
	{
		LogNetworkMessage("Client network interface started successfully.");
		LogNetworkMessage("Attempting to connect to server.");
		RakNet::ConnectionAttemptResult connectResult = m_peerInterface->Connect(SERVER_IP, PORT, nullptr, 0);

		if (connectResult == RakNet::ConnectionAttemptResult::CONNECTION_ATTEMPT_STARTED)
			result = true;
		else
			LogFailedConnection(connectResult);
	}
	else
	{
		LogFailedStartup(startResult, sd);
	}

	return result;
}

void MyApplication::HandleNetworkMessages()
{
	RakNet::Packet* packet;

	for (packet = m_peerInterface->Receive(); packet;
		m_peerInterface->DeallocatePacket(packet),
		packet = m_peerInterface->Receive())
	{
		switch (packet->data[0])
		{
			case ID_REMOTE_DISCONNECTION_NOTIFICATION:
			{
				HandleRemoteDisconnection();
				break;
			}
			case ID_REMOTE_CONNECTION_LOST:
			{
				HandleRemoteLostConnection();
				break;
			}
			case ID_REMOTE_NEW_INCOMING_CONNECTION:
			{
				HandleRemoteNewConnection();
				break;
			}
			case ID_CONNECTION_REQUEST_ACCEPTED:
			{
				HandleConnectRequestAccepted();
				break;
			}
			case ID_NO_FREE_INCOMING_CONNECTIONS:
			{
				HandleNoFreeConnections();
				break;
			}
			case ID_DISCONNECTION_NOTIFICATION:
			{
				HandleDisconnection();
				break;
			}
			case ID_CONNECTION_LOST:
			{
				HandleLostConnection();
				break;
			}
			case ID_SERVER_GAME_CLIENT_ID:
			{
				RakNet::BitStream bs(packet->data, packet->length, false);
				bs.IgnoreBytes(sizeof(RakNet::MessageID));
				HandleNewClientID(bs, packet->systemAddress);
				break;
			}
			case ID_SERVER_GAME_CREATE_PLAYER:
			{
				RakNet::BitStream bs(packet->data, packet->length, false);
				bs.IgnoreBytes(sizeof(RakNet::MessageID));
				HandleCreatePlayer(bs, packet->systemAddress);
				break;
			}
			case ID_SERVER_GAME_CREATE_BULLET:
			{
				RakNet::BitStream bs(packet->data, packet->length, false);
				bs.IgnoreBytes(sizeof(RakNet::MessageID));
				HandleCreateBullet(bs, packet->systemAddress);
				break;
			}
			case ID_SERVER_GAME_UPDATE_PLAYER_POSITION:
			{
				RakNet::BitStream bs(packet->data, packet->length, false);
				bs.IgnoreBytes(sizeof(RakNet::MessageID));
				HandleUpdatePlayerPos(bs, packet->systemAddress);
				break;
			}
			case ID_SERVER_GAME_UPDATE_PLAYER_VELOCITY:
			{
				RakNet::BitStream bs(packet->data, packet->length, false);
				bs.IgnoreBytes(sizeof(RakNet::MessageID));
				HandleUpdatePlayerVel(bs, packet->systemAddress);
				break;
			}
			default:
			{
				LogNetworkMessage("Received a message with an unknown id: " + packet->data[0]);
				break;
			}
		}
	}
}

void MyApplication::HandleRemoteDisconnection()
{
	LogNetworkMessage("Another client has disconnected.");
}

void MyApplication::HandleRemoteLostConnection()
{
	LogNetworkMessage("Another client has lost connection.");
}

void MyApplication::HandleRemoteNewConnection()
{
	LogNetworkMessage("Another client has connected.");
}

void MyApplication::HandleConnectRequestAccepted()
{
	LogNetworkMessage("Our connection request has been accepted.");
}

void MyApplication::HandleNoFreeConnections()
{
	LogNetworkMessage("The server is full.");
}

void MyApplication::HandleDisconnection()
{
	LogNetworkMessage("We have been disconnected.");
}

void MyApplication::HandleLostConnection()
{
	LogNetworkMessage("Connection lost.");
}

void MyApplication::HandleNewClientID(RakNet::BitStream& bs, RakNet::SystemAddress& ownerSysAddress)
{
	LogNetworkMessage("Server has given us an id of: " + m_clientID);
}

void MyApplication::HandleCreatePlayer(RakNet::BitStream& bs, RakNet::SystemAddress& ownerSysAddress)
{

}

void MyApplication::HandleCreateBullet(RakNet::BitStream& bs, RakNet::SystemAddress& ownerSysAddress)
{

}

void MyApplication::HandleUpdatePlayerPos(RakNet::BitStream& bs, RakNet::SystemAddress& ownerSysAddress)
{

}

void MyApplication::HandleUpdatePlayerVel(RakNet::BitStream& bs, RakNet::SystemAddress& ownerSysAddress)
{

}

void MyApplication::LogNetworkMessage(const char* message)
{
	std::cout << message << std::endl;
}

void MyApplication::LogFailedStartup(RakNet::StartupResult startResult, RakNet::SocketDescriptor& sd)
{
	LogNetworkMessage("Failed to start server.");
	std::string errorTag;

	switch (startResult)
	{
		case RakNet::StartupResult::COULD_NOT_GENERATE_GUID:
			errorTag = "COULD_NOT_GENERATE_GUID"; break;
		case RakNet::StartupResult::FAILED_TO_CREATE_NETWORK_THREAD:
			errorTag = "FAILED_TO_CREATE_NETWORK_THREAD"; break;
		case RakNet::StartupResult::INVALID_MAX_CONNECTIONS:
			errorTag = "INVALID_MAX_CONNECTIONS"; break;
		case RakNet::StartupResult::INVALID_SOCKET_DESCRIPTORS:
			errorTag = "INVALID_SOCKET_DESCRIPTORS"; break;
		case RakNet::StartupResult::PORT_CANNOT_BE_ZERO:
			errorTag = "PORT_CANNOT_BE_ZERO"; break;
		case RakNet::StartupResult::RAKNET_ALREADY_STARTED:
			errorTag = "RAKNET_ALREADY_STARTED"; break;
		case RakNet::StartupResult::SOCKET_FAILED_TEST_SEND:
			errorTag = "SOCKET_FAILED_TEST_SEND"; break;
		case RakNet::StartupResult::SOCKET_FAILED_TO_BIND:
			errorTag = "SOCKET_FAILED_TO_BIND"; break;
		case RakNet::StartupResult::SOCKET_FAMILY_NOT_SUPPORTED:
			errorTag = "SOCKET_FAMILY_NOT_SUPPORTED"; break;
		case RakNet::StartupResult::SOCKET_PORT_ALREADY_IN_USE:
			errorTag = "SOCKET_PORT_ALREADY_IN_USE"; break;
		case RakNet::StartupResult::STARTUP_OTHER_FAILURE:
			errorTag = "STARTUP_OTHER_FAILURE"; break;
		default:
			errorTag = "UNKNOWN"; break;
	}

	std::string msg = "";
	msg += "--------------------------------------------------------" + '\n';
	msg += "StartupResult Error: " + errorTag + '\n';
	msg += "--------------------------------------------------------" + '\n';
	msg += "PORT: " + PORT + '\n';
	msg += "sd.blockingSocket: " + sd.blockingSocket + '\n';
	msg += "sd.chromeInstance: " + sd.chromeInstance + '\n';
	msg += "sd.extraSocketOptions: " + sd.extraSocketOptions + '\n';
	msg += "sd.hostAddress: " + std::string(sd.hostAddress) + '\n';
	msg += "sd.port: " + sd.port + '\n';
	msg += "sd.remotePortRakNetWasStartedOn_PS3_PSP2: " + sd.remotePortRakNetWasStartedOn_PS3_PSP2 + '\n';
	msg += "sd.socketFamily: " + sd.socketFamily + '\n';
	msg += "--------------------------------------------------------" + '\n';

	LogNetworkMessage(msg.c_str());
}

void MyApplication::LogFailedConnection(RakNet::ConnectionAttemptResult connectResult)
{
	std::string errorTag;

	switch (connectResult)
	{
	case RakNet::ConnectionAttemptResult::ALREADY_CONNECTED_TO_ENDPOINT:
		errorTag = "ALREADY_CONNECTED_TO_ENDPOINT"; break;
	case RakNet::ConnectionAttemptResult::CANNOT_RESOLVE_DOMAIN_NAME:
		errorTag = "CANNOT_RESOLVE_DOMAIN_NAME"; break;
	case RakNet::ConnectionAttemptResult::CONNECTION_ATTEMPT_ALREADY_IN_PROGRESS:
		errorTag = "CONNECTION_ATTEMPT_ALREADY_IN_PROGRESS"; break;
	case RakNet::ConnectionAttemptResult::INVALID_PARAMETER:
		errorTag = "INVALID_PARAMETER"; break;
	case RakNet::ConnectionAttemptResult::SECURITY_INITIALIZATION_FAILED:
		errorTag = "SECURITY_INITIALIZATION_FAILED"; break;
	default:
		errorTag = "UNKNOWN"; break;
	}

	std::string msg = "";
	msg += "--------------------------------------------------------" + '\n';
	msg += "Server IP: " + std::string(SERVER_IP) + '\n';
	msg += "Port: " + PORT + '\n';
	msg += "ConnectionResult tag: " + errorTag + '\n';
	msg += "--------------------------------------------------------" + '\n';

	LogNetworkMessage(msg.c_str());
}