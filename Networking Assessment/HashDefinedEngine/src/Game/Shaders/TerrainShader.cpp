#include "Game\Shaders\TerrainShader.h"

//const char* const TerrainShader::VERT_FILE = "Terrain.vert";
//const char* const TerrainShader::FRAG_FILE = "Terrain.frag";

TerrainShader::TerrainShader(unsigned int grassTextureID, unsigned int rockTextureID)
	: Shader("Terrain.vert", "Terrain.frag")
{
	m_grassTextureID = grassTextureID;
	m_rockTextureID = rockTextureID;

	m_locationTileSize = -1;
	m_locationGrassTexture = -1;
	m_locationRockTexture = -1;

	m_locationProjectionView = -1;
	m_locationCameraPos = -1;
	m_locationLightColour = -1;
	m_locationLightDirection = -1;
	m_locationMaxGrassHeight = -1;
	m_locationMinRockHeight = -1;

	CreateProgram();
}

TerrainShader::~TerrainShader()
{
}

void TerrainShader::BindAttributes()
{
	BindAttribute(0, "position");
	BindAttribute(1, "normal");
	BindAttribute(2, "texcoord");
}

void TerrainShader::GetAllUniformLocations()
{
	m_locationProjectionView = GetUniformLocation("projView");
	m_locationCameraPos = GetUniformLocation("cameraPosition");
	m_locationLightColour = GetUniformLocation("lightColour");
	m_locationLightDirection = GetUniformLocation("lightDir");
	m_locationMaxGrassHeight = GetUniformLocation("maxGrassHeight");
	m_locationMinRockHeight = GetUniformLocation("minRockHeight");
	m_locationGrassTexture = GetUniformLocation("grass");
	m_locationRockTexture = GetUniformLocation("rock");
	m_locationTileSize = GetUniformLocation("tileSize");
}

void TerrainShader::LoadProjectionViewMatrix(const glm::mat4& projView)
{
	LoadMat4(m_locationProjectionView, projView);
}

void TerrainShader::LoadCameraPosition(const glm::vec3& pos)
{
	LoadVec3(m_locationCameraPos, pos);
}

void TerrainShader::LoadLightColour(const glm::vec4& colour)
{
	LoadVec4(m_locationLightColour, colour);
}

void TerrainShader::LoadLightDirection(const glm::vec3& direction)
{
	LoadVec3(m_locationLightDirection, direction);
}

void TerrainShader::LoadMaxGrassHeight(float val)
{
	LoadFloat(m_locationMaxGrassHeight, val);
}

void TerrainShader::LoadMinRockHeight(float val)
{
	LoadFloat(m_locationMinRockHeight, val);
}

void TerrainShader::LoadTileSize(const glm::vec2& tileSize)
{
	LoadVec2(m_locationTileSize, tileSize);
}

void TerrainShader::LoadTextures()
{
	LoadSampler2D(m_locationGrassTexture, 1, m_grassTextureID);
	LoadSampler2D(m_locationRockTexture, 2, m_rockTextureID);
}