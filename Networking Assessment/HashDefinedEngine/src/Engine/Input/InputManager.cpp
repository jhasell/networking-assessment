#include "Engine\Input\InputManager.h"
#include "Engine\BaseApplication.h"

InputManager::InputManager(BaseApplication* app)
{
	m_app = app;
}

bool InputManager::IsKeyDown(int key)
{
	return (glfwGetKey(m_app->GetWindow(), key) == GLFW_PRESS);
}

bool InputManager::IsMouseButtonDown(int btn)
{
	return (glfwGetMouseButton(m_app->GetWindow(), btn) == GLFW_PRESS);
}

glm::vec2 InputManager::GetMousePos()
{
	double mx = 0, my = 0;
	glfwGetCursorPos(m_app->GetWindow(), &mx, &my);

	return glm::vec2((float)mx, (float)my);
}
