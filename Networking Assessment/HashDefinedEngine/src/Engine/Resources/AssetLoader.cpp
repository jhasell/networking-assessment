#include "Engine\Resources\AssetLoader.h"

#include "AIE\gl_core_4_4.h"
#include "GLFW\glfw3.h"

//#define STB_IMAGE_IMPLEMENTATION
#include "FBXLoader\FBXFile.h"
#include <stb\stb_image.h>

#include "glm\glm.hpp"
#include "glm\ext.hpp"

#include "Engine\Physics\Collider\AABB.h"
#include "Engine\Physics\Collider\boundingSphere.h"


const char* AssetLoader::TEX_REL_PATH = "textures\\";
const char* AssetLoader::VERT_REL_PATH = "shaders\\vertex\\";
const char* AssetLoader::FRAG_REL_PATH = "shaders\\fragment\\";
const char* AssetLoader::GEOM_REL_PATH = "shaders\\geometry\\";
const char* AssetLoader::TESS_CONT_REL_PATH = "shaders\\tesscontrol\\";
const char* AssetLoader::TESS_EVAL_REL_PATH = "shaders\\tessevaluation\\";

FBXModel* AssetLoader::LoadFBXObjFromFile(const char* fbxFile)
{
	std::string path = GetResourceFolder() + FBXModel::FBX_RELATIVE_PATH + fbxFile;

	FBXFile* fbx = new FBXFile();
	fbx->load(path.c_str());

	for (unsigned int i = 0; i < fbx->getMeshCount(); ++i)
	{
		FBXMeshNode* mesh = fbx->getMeshByIndex(i);
	
		RendererData::BufferData vBuffer(mesh->m_vertices.size() * sizeof(FBXVertex), 
			mesh->m_vertices.data(), GL_STATIC_DRAW);
		RendererData::BufferData iBuffer(mesh->m_indices.size() * sizeof(unsigned int),
			mesh->m_indices.data(), GL_STATIC_DRAW);
	
		std::vector<RendererData::AttributeData> attribs;
		attribs.push_back(RendererData::AttributeData(
			0, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), (void*)FBXVertex::PositionOffset));	// position
		attribs.push_back(RendererData::AttributeData(
			1, 4, GL_FLOAT, GL_TRUE, sizeof(FBXVertex), (void*)FBXVertex::NormalOffset));		// normals
		attribs.push_back(RendererData::AttributeData(
			2, 4, GL_FLOAT, GL_TRUE, sizeof(FBXVertex), (void*)FBXVertex::TangentOffset));		// tangents
		attribs.push_back(RendererData::AttributeData(
			3, 2, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), (void*)FBXVertex::TexCoord1Offset));	// tex coords
		attribs.push_back(RendererData::AttributeData(
			4, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), (void*)FBXVertex::WeightsOffset));		// weights
		attribs.push_back(RendererData::AttributeData(
			5, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), (void*)FBXVertex::IndicesOffset));		// indices
	
		RendererData* rendererData = new RendererData();
		rendererData->Init(vBuffer, iBuffer, attribs, mesh->m_indices.size());
		mesh->m_userData = rendererData;
	}

	FBXMeshNode* mesh = fbx->getMeshByIndex(0);
	AABB* collider = new AABB();
	std::vector<glm::vec3> verts;
	for (unsigned int i = 0; i < mesh->m_vertices.size(); i++)
	{
		glm::vec4 vertPos = mesh->m_vertices[i].position;
		verts.push_back(glm::vec3(vertPos.x, vertPos.y, vertPos.z));
	}

	collider->Fit(verts);

	return new FBXModel(fbx, collider);
}

Mesh* AssetLoader::LoadObjFromFile(const char* objFile)
{
	//std::string path = GetResourceFolder() + Model::OBJ_RELATIVE_PATH + objFile;






	/*
	// Load Obj Models with TinyObjLoader
	static const char* modelFile = "Buddha.obj";
	std::string modelPath = Loader::GetResourceFolder() + "models\\obj\\" + modelFile;

	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;
	std::string err;
	if (tinyobj::LoadObj(shapes, materials, err, modelPath.c_str()) == false)
	{
	std::cerr << "Unable to load object \'" + modelPath + "\'." << std::endl;
	return false;
	}

	CreateOpenGLBuffers(shapes);
	*/

	return nullptr;
}

unsigned int AssetLoader::LoadShader(unsigned int type, const char* filename)
{
	if (filename == nullptr)
		return 0;

	// Get actual shader file path
	std::string fullPath = AssetLoader::GetResourceFolder();

	switch (type)
	{
	case GL_VERTEX_SHADER:
		fullPath += VERT_REL_PATH;
		break;
	case GL_FRAGMENT_SHADER:
		fullPath += FRAG_REL_PATH;
		break;
	case GL_GEOMETRY_SHADER:
		fullPath += GEOM_REL_PATH;
		break;
	case GL_TESS_CONTROL_SHADER:
		fullPath += TESS_CONT_REL_PATH;
		break;
	case GL_TESS_EVALUATION_SHADER:
		fullPath += TESS_EVAL_REL_PATH;
		break;

	default:
		return 0;
	}

	fullPath += filename;

	// Load shader
	std::string contents = AssetLoader::ReadFile(fullPath);
	const char* source = contents.c_str();

	unsigned int shader = glCreateShader(type);
	glShaderSource(shader, 1, &source, 0);
	glCompileShader(shader);

	// Check if compile was successful
	GLint result = 0;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &result);
	if (result == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);

		//std::vector<GLchar> errorLog(maxLength);
		char* errorLog = new char[maxLength];

		glGetShaderInfoLog(shader, maxLength, &maxLength, errorLog);
		printf("Error: Failed to compile shader: %s\n", filename);
		printf("%s\n", errorLog);

		glDeleteShader(shader);
		return 0;
	}

	return shader;
}

unsigned int AssetLoader::LoadTexture(const char* filename)
{
	std::string fullPath = GetResourceFolder() + TEX_REL_PATH + filename;
	
	int width = 0, height = 0, format = 0;
	unsigned char* data = stbi_load(fullPath.c_str(), &width, &height, &format, STBI_default);
	
	unsigned int textureID;
	glGenTextures(1, &textureID);
	glBindTexture(GL_TEXTURE_2D, textureID);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height,
		0, GL_RGB, GL_UNSIGNED_BYTE, data);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); //GL_CLAMP_TO_EDGE
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); //GL_CLAMP_TO_EDGE
	glBindTexture(GL_TEXTURE_2D, 0);

	delete data;
		
	return textureID;
}

Mesh* AssetLoader::GenerateGrid(unsigned int rows, unsigned int cols, float spacing)
{
	// Make sure parameters are valid
	if (rows <= 0 || cols <= 0)
		return nullptr;

	// Create vertex's
	unsigned int vertListSize = rows * cols;
	Vertex* aoVertices = new Vertex[vertListSize];
	float offsetX = (cols * spacing) / 2.0f;
	float offsetZ = (rows * spacing) / 2.0f;

	for (unsigned int r = 0; r < rows; r++)
	{
		for (unsigned int c = 0; c < cols; c++)
		{
			unsigned int index = r * cols + c;
			aoVertices[index].position = glm::vec3((float)c - offsetX + (spacing * c), 0, (float)r - offsetZ + (spacing * r));
			aoVertices[index].normal = glm::vec3(0, 1, 0);
			aoVertices[index].texcoord = glm::vec2((1.0f / rows) * r, (1.0f / cols) * c);

			//aoVertices[r * cols + c].position.x = (float)c - offsetX;
			//aoVertices[r * cols + c].position.y = 0;
			//aoVertices[r * cols + c].position.z = (float)r - offsetZ;
			//aoVertices[r * cols + c].position.w = 1;

			//aoVertices[r * cols + c].texcoord.x = (1.0f / rows) * r;
			//aoVertices[r * cols + c].texcoord.y = (1.0f / cols) * c;
		}
	}

	// Create Indices
	unsigned int indexCount = (rows - 1) * (cols - 1) * 6;
	unsigned int* auiIndices = new unsigned int[indexCount];
	unsigned int index = 0;

	for (unsigned int r = 0; r < (rows - 1); r++)
	{
		for (unsigned int c = 0; c < (cols - 1); c++)
		{
			auiIndices[index++] = r * cols + c;
			auiIndices[index++] = (r + 1) * cols + c;
			auiIndices[index++] = (r + 1) * cols + (c + 1);

			auiIndices[index++] = r * cols + c;
			auiIndices[index++] = (r + 1) * cols + (c + 1);
			auiIndices[index++] = r * cols + (c + 1);
		}
	}

	// Create render data
	RendererData::BufferData vBuffer((rows * cols) * sizeof(Vertex), aoVertices, GL_STATIC_DRAW);
	RendererData::BufferData iBuffer(indexCount * sizeof(unsigned int), auiIndices, GL_STATIC_DRAW);

	void* normalOffset = (void*)(sizeof(glm::vec3));
	void* uvOffset = (void*)(sizeof(glm::vec3) * 2);
	std::vector<RendererData::AttributeData> attribs;
	attribs.push_back(RendererData::AttributeData(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0));
	attribs.push_back(RendererData::AttributeData(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), normalOffset));
	attribs.push_back(RendererData::AttributeData(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), uvOffset));

	RendererData* rendererData = new RendererData();
	rendererData->Init(vBuffer, iBuffer, attribs, indexCount);

	AABB* collider = new AABB();
	std::vector<glm::vec3> verts;
	for (unsigned int i = 0; i < vertListSize; i++)
		verts.push_back(aoVertices[i].position);

	collider->Fit(verts);

	// Cleanup
	delete[] aoVertices;
	delete[] auiIndices;

	return new Mesh(rendererData, collider);
}

Mesh* AssetLoader::GenerateGrid(unsigned int rows, unsigned int cols, float* heightData, float spacing)
{
	// Make sure parameters are valid
	if (rows <= 0 || cols <= 0)
		return nullptr;

	unsigned int vertListSize = rows*cols;

	Vertex* aoVertices = new Vertex[vertListSize];
	float offsetX = (cols * spacing) / 2.0f;
	float offsetZ = (rows * spacing) / 2.0f;

	// Create vertices & texture coords
	for (unsigned int r = 0; r < rows; r++)
	{
		for (unsigned int c = 0; c < cols; c++)
		{
			unsigned int index = r * cols + c;

			// Get vert position
			aoVertices[index].position = glm::vec3(
				(float)c - offsetX + (spacing * c), 
				heightData[index] * 50,
				(float)r - offsetZ + (spacing * r));

			// Get vert texture coords
			aoVertices[index].texcoord = glm::vec2(
				(1.0f / rows) * r, 
				(1.0f / cols) * c);

			//aoVertices[r * cols + c].position.x = (float)c - offsetX;
			//aoVertices[r * cols + c].position.y = 0;
			//aoVertices[r * cols + c].position.z = (float)r - offsetZ;
			//aoVertices[r * cols + c].position.w = 1;

			//aoVertices[r * cols + c].texcoord.x = (1.0f / rows) * r;
			//aoVertices[r * cols + c].texcoord.y = (1.0f / cols) * c;
		}
	}

	// Create Normals
	for (unsigned int r = 0; r < rows; r++)
	{
		for (unsigned int c = 0; c < cols; c++)
		{
			unsigned int index = r * cols + c;
			
			glm::vec3 currPos  = aoVertices[index].position;
			glm::vec3 upPos    = aoVertices[index - cols].position;
			glm::vec3 downPos  = aoVertices[index + cols].position;
			glm::vec3 leftPos  = aoVertices[index - 1].position;
			glm::vec3 rightPos = aoVertices[index + 1].position;


			glm::vec3 dirU = upPos - currPos;
			glm::vec3 dirD = downPos - currPos;
			glm::vec3 dirL = leftPos - currPos;
			glm::vec3 dirR = rightPos - currPos;

			glm::vec3 crossRU = glm::vec3(0);
			glm::vec3 crossUL = glm::vec3(0);
			glm::vec3 crossLD = glm::vec3(0);
			glm::vec3 crossDR = glm::vec3(0);

			if (r == 0)
			{
				if (c == 0)
					crossDR = glm::cross(dirD, dirR);
				else if(c == (cols - 1))
					crossLD = glm::cross(dirL, dirD);
				else
				{
					crossDR = glm::cross(dirD, dirR);
					crossLD = glm::cross(dirL, dirD);
				}
			}
			else if (r == (rows - 1))
			{
				if (c == 0)
					crossRU = glm::cross(dirR, dirU);
				else if (c == (cols - 1))
					crossUL = glm::cross(dirU, dirL);
				else
				{
					crossRU = glm::cross(dirR, dirU);
					crossUL = glm::cross(dirU, dirL);
				}
			}
			else if (c == 0)
			{
				crossRU = glm::cross(dirR, dirU);
				crossDR = glm::cross(dirD, dirR);
			}
			else if (c == (cols - 1))
			{
				crossLD = glm::cross(dirL, dirD);
				crossUL = glm::cross(dirU, dirL);
			}
			else
			{
				crossRU = glm::cross(dirR, dirU);
				crossDR = glm::cross(dirD, dirR);
				crossLD = glm::cross(dirL, dirD);
				crossUL = glm::cross(dirU, dirL);
			}

			glm::vec3 combinedNorm = crossRU + crossDR + crossLD + crossUL;
			aoVertices[index].normal = glm::normalize(combinedNorm);
		}
	}

	// Create Indices
	unsigned int indexCount = (rows - 1) * (cols - 1) * 6;
	unsigned int* auiIndices = new unsigned int[indexCount];
	unsigned int index = 0;

	for (unsigned int r = 0; r < (rows - 1); r++)
	{
		for (unsigned int c = 0; c < (cols - 1); c++)
		{
			auiIndices[index++] = r * cols + c;
			auiIndices[index++] = (r + 1) * cols + c;
			auiIndices[index++] = (r + 1) * cols + (c + 1);

			auiIndices[index++] = r * cols + c;
			auiIndices[index++] = (r + 1) * cols + (c + 1);
			auiIndices[index++] = r * cols + (c + 1);
		}
	}

	// Create render data
	RendererData::BufferData vBuffer((rows * cols) * sizeof(Vertex), aoVertices, GL_STATIC_DRAW);
	RendererData::BufferData iBuffer(indexCount * sizeof(unsigned int), auiIndices, GL_STATIC_DRAW);

	void* normalOffset = (void*)(sizeof(glm::vec3));
	void* uvOffset = (void*)(sizeof(glm::vec3) * 2);
	std::vector<RendererData::AttributeData> attribs;
	attribs.push_back(RendererData::AttributeData(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0));
	attribs.push_back(RendererData::AttributeData(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), normalOffset));
	attribs.push_back(RendererData::AttributeData(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), uvOffset));

	RendererData* rendererData = new RendererData();
	rendererData->Init(vBuffer, iBuffer, attribs, indexCount);

	
	AABB* collider = new AABB();
	std::vector<glm::vec3> verts;
	for (unsigned int i = 0; i < vertListSize; i++)
		verts.push_back(aoVertices[i].position);

	collider->Fit(verts);

	// Cleanup
	delete[] aoVertices;
	delete[] auiIndices;

	return new Mesh(rendererData, collider);
}

Mesh* AssetLoader::GenerateSphere(glm::vec3& center, float radius, unsigned int rows, unsigned int cols, glm::vec4& fillColour)
{
	float longMax = 360;
	float latMin = -90;
	float latMax = 90;

	float invRadius = 1.0f / radius;

	// Invert these first as the multiply is slightly quicker
	float invRows = 1.0f / (float)rows;
	float invCols = 1.0f / (float)cols;

	float deg2Rad = glm::pi<float>() / 180;

	// Put everything into radians
	float latRange = (latMax - latMin) * deg2Rad;
	float longRange = longMax * deg2Rad;

	// Create vertex array
	int vertCount = (rows + 1) * (cols + 1);
	Vertex* vertices = new Vertex[vertCount];

	for (unsigned int r = 0; r <= rows; r++)
	{
		// Y coords, rotating around X-Axis
		float ratioAroundXAxis = (float)r * invRows;
		float radiansAroundXAxis = ratioAroundXAxis * latRange + (latMin * deg2Rad);
		float y = radius * sin(radiansAroundXAxis);
		float z = radius * cos(radiansAroundXAxis);

		for (unsigned int c = 0; c <= cols; c++)
		{
			float ratioAroundYAxis = (float)c * invCols;
			float theta = ratioAroundYAxis * longRange;
			glm::vec3 pos(center.x + (-z * sinf(theta)), center.y + (y), center.z + (-z * cosf(theta)));
			glm::vec3 d = center - glm::vec3(pos.x, pos.y, pos.z); // vec from center to pos
			glm::vec3 norm = glm::normalize(d);

			//float u = 0.5f + (atan2f(d.z, d.x) / (2.0f * glm::pi<float>()));
			//float v = 0.5f - (asinf(d.y) / glm::pi<float>());
			//float u = (atan2f(pos.z, pos.x) / (glm::pi<float>() / 2.0f));
			//float v = (acosf(pos.y / radius) / glm::pi<float>());
			float u = (1.0f / rows) * r;
			float v = (1.0f / cols) * c;
			
			unsigned int index = r * cols + (c % cols);
			vertices[index].position = pos;
			vertices[index].normal = glm::vec3(norm.x, norm.y, norm.z);
			vertices[index].texcoord = glm::vec2(u, v);
		}
	}

	int quadCount = rows * cols;
	int indicesSize = quadCount * 6;
	unsigned int* pIndices = new unsigned int[indicesSize];
	int index = 0;
	
	for (int quadIndex = 0; quadIndex < quadCount; quadIndex++)
	{
		int nextQuad = quadIndex + 1;

		if (nextQuad % cols == 0)
			nextQuad = nextQuad - cols;

		if (quadIndex % cols == 0 && longRange < (glm::pi<float>() * 2))
			continue;

		// Triangle 1
		pIndices[index++] = nextQuad + cols;
		pIndices[index++] = quadIndex;
		pIndices[index++] = nextQuad;

		// Triangle 2
		pIndices[index++] = nextQuad + cols;
		pIndices[index++] = quadIndex + cols;
		pIndices[index++] = quadIndex;
	}

	// Create render data
	RendererData::BufferData vBuffer(vertCount * sizeof(Vertex), vertices, GL_STATIC_DRAW);
	RendererData::BufferData iBuffer(indicesSize * sizeof(unsigned int), pIndices, GL_STATIC_DRAW);

	void* normOffset = (void*)(sizeof(glm::vec3));
	void* uvOffset = (void*)(sizeof(glm::vec3) * 2);
	std::vector<RendererData::AttributeData> attribs;
	attribs.push_back(RendererData::AttributeData(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0));
	attribs.push_back(RendererData::AttributeData(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), normOffset));
	attribs.push_back(RendererData::AttributeData(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), uvOffset));

	RendererData* rendererData = new RendererData();
	rendererData->Init(vBuffer, iBuffer, attribs, indicesSize);

	
	BoundingSphere* collider = new BoundingSphere();
	std::vector<glm::vec3> verts;
	for (int i = 0; i < vertCount; i++)
		verts.push_back(vertices[i].position);

	collider->Fit(verts);

	// cleanup
	delete vertices;

	return new Mesh(rendererData, collider);
}

Mesh* AssetLoader::GenerateQuad()
{
	Vertex vertexData[4];
	vertexData[0].position = glm::vec3(-5, 0, 5);
	vertexData[0].normal = glm::vec3(0, 1, 0);
	vertexData[0].texcoord = glm::vec2(0, 1);
	
	vertexData[1].position = glm::vec3(5, 0, 5);
	vertexData[1].normal = glm::vec3(0, 1, 0);
	vertexData[1].texcoord = glm::vec2(1, 1);
	
	vertexData[2].position = glm::vec3(5, 0, -5);
	vertexData[2].normal = glm::vec3(0, 1, 0);
	vertexData[2].texcoord = glm::vec2(1, 0);
	
	vertexData[3].position = glm::vec3(-5, 0, -5);
	vertexData[3].normal = glm::vec3(0, 1, 0);
	vertexData[3].texcoord = glm::vec2(0, 0);

	unsigned int indexData[] =
	{
		0,1,2,
		0,2,3,
	};

	// Create render data
	RendererData::BufferData vBuffer(sizeof(Vertex) * 4, vertexData, GL_STATIC_DRAW);
	RendererData::BufferData iBuffer(sizeof(unsigned int) * 6, indexData, GL_STATIC_DRAW);

	void* normOffset = (void*)(sizeof(glm::vec3));
	void* uvOffset = (void*)(sizeof(glm::vec3) * 2);
	std::vector<RendererData::AttributeData> attribs;
	attribs.push_back(RendererData::AttributeData(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0));
	attribs.push_back(RendererData::AttributeData(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), normOffset));
	attribs.push_back(RendererData::AttributeData(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), uvOffset));

	RendererData* rendererData = new RendererData();
	rendererData->Init(vBuffer, iBuffer, attribs, 6);

	AABB* collider = new AABB();
	std::vector<glm::vec3> verts;
	for (auto vData : vertexData)
		verts.push_back(vData.position);

	collider->Fit(verts);

	return new Mesh(rendererData, collider);
}

Mesh* AssetLoader::GenerateCube()
{
	Vertex vertexData[24];

	float x = 5;
	float z = 5;
	float y = 5;

	//---------------------------------------------------------//
	// vertex 0
	// face 0
	vertexData[0].position = glm::vec3(-x, -y, z);
	vertexData[0].normal = glm::vec3(0, -1, 0);
	vertexData[0].texcoord = glm::vec2(0, 0);

	// vertex 0
	// face 2
	vertexData[1].position = glm::vec3(-x, -y, z);
	vertexData[1].normal = glm::vec3(-1, 0, 0);
	vertexData[1].texcoord = glm::vec2(0, 0);

	// vertex 0
	// face 3
	vertexData[2].position = glm::vec3(-x, -y, z);
	vertexData[2].normal = glm::vec3(0, 0, 1);
	vertexData[2].texcoord = glm::vec2(1, 0);

	//---------------------------------------------------------//
	// vertex 1
	// face 0
	vertexData[3].position = glm::vec3(x, -y, z);
	vertexData[3].normal = glm::vec3(0, -1, 0);
	vertexData[3].texcoord = glm::vec2(1, 0);

	// vertex 1
	// face 3
	vertexData[4].position = glm::vec3(x, -y, z);
	vertexData[4].normal = glm::vec3(0, 0, 1);
	vertexData[4].texcoord = glm::vec2(0, 0);

	// vertex 1
	// face 4
	vertexData[5].position = glm::vec3(x, -y, z);
	vertexData[5].normal = glm::vec3(1, 0, 0);
	vertexData[5].texcoord = glm::vec2(1, 0);

	//---------------------------------------------------------//
	// vertex 2
	// face 0
	vertexData[6].position = glm::vec3(x, -y, -z);
	vertexData[6].normal = glm::vec3(0, -1, 0);
	vertexData[6].texcoord = glm::vec2(1, 1);

	// vertex 2
	// face 1
	vertexData[7].position = glm::vec3(x, -y, -z);
	vertexData[7].normal = glm::vec3(0, 0, -1);
	vertexData[7].texcoord = glm::vec2(1, 0);

	// vertex 2
	// face 4
	vertexData[8].position = glm::vec3(x, -y, -z);
	vertexData[8].normal = glm::vec3(1, 0, 0);
	vertexData[8].texcoord = glm::vec2(0, 0);

	//---------------------------------------------------------//
	// vertex 3
	// face 0
	vertexData[9].position = glm::vec3(-x, -y, -z);
	vertexData[9].normal = glm::vec3(0, -1, 0);
	vertexData[9].texcoord = glm::vec2(0, 1);

	// vertex 3
	// face 1
	vertexData[10].position = glm::vec3(-x, -y, -z);
	vertexData[10].normal = glm::vec3(0, 0, -1);
	vertexData[10].texcoord = glm::vec2(0, 0);

	// vertex 3
	// face 2
	vertexData[11].position = glm::vec3(-x, -y, -z);
	vertexData[11].normal = glm::vec3(-1, 0, 0);
	vertexData[11].texcoord = glm::vec2(1, 0);

	//---------------------------------------------------------//
	// vertex 4
	// face 2
	vertexData[12].position = glm::vec3(-x, y, z);
	vertexData[12].normal = glm::vec3(-1, 0, 0);
	vertexData[12].texcoord = glm::vec2(0, 1);

	// vertex 4
	// face 3
	vertexData[13].position = glm::vec3(-x, y, z);
	vertexData[13].normal = glm::vec3(0, 0, 1);
	vertexData[13].texcoord = glm::vec2(1, 1);

	// vertex 4
	// face 5
	vertexData[14].position = glm::vec3(-x, y, z);
	vertexData[14].normal = glm::vec3(0, 1, 0);
	vertexData[14].texcoord = glm::vec2(0, 1);

	//---------------------------------------------------------//
	// vertex 5
	// face 3
	vertexData[15].position = glm::vec3(x, y, z);
	vertexData[15].normal = glm::vec3(0, 0, 1);
	vertexData[15].texcoord = glm::vec2(0, 1);

	// vertex 5
	// face 4
	vertexData[16].position = glm::vec3(x, y, z);
	vertexData[16].normal = glm::vec3(1, 0, 0);
	vertexData[16].texcoord = glm::vec2(1, 1);

	// vertex 5
	// face 5
	vertexData[17].position = glm::vec3(x, y, z);
	vertexData[17].normal = glm::vec3(0, 1, 0);
	vertexData[17].texcoord = glm::vec2(1, 1);

	//---------------------------------------------------------//
	// vertex 6
	// face 1
	//vertexData[23] = { glm::vec4(), glm::vec4() , glm::vec2() };
	vertexData[18].position = glm::vec3(x, y, -z);
	vertexData[18].normal = glm::vec3(0, 0, -1);
	vertexData[18].texcoord = glm::vec2(1, 1);

	// vertex 6
	// face 4
	//vertexData[23] = { glm::vec4(), glm::vec4() , glm::vec2() };
	vertexData[19].position = glm::vec3(x, y, -z);
	vertexData[19].normal = glm::vec3(1, 0, 0);
	vertexData[19].texcoord = glm::vec2(0, 1);

	// vertex 6
	// face 5
	//vertexData[23] = { glm::vec4(x, y, -z, w), glm::vec4(0, 1, 0, 0) , glm::vec2(1, 0) };
	vertexData[20].position = glm::vec3(x, y, -z);
	vertexData[20].normal = glm::vec3(0, 1, 0);
	vertexData[20].texcoord = glm::vec2(1, 0);

	//---------------------------------------------------------//
	// vertex 7
	// face 1
	//vertexData[23] = { glm::vec4(-x, y, -z, w), glm::vec4(0, 0, -1, 0) , glm::vec2(0, 1) };
	vertexData[21].position = glm::vec3(-x, y, -z);
	vertexData[21].normal = glm::vec3(0, 0, -1);
	vertexData[21].texcoord = glm::vec2(0, 1);

	// vertex 7
	// face 2
	//vertexData[23] = { glm::vec4(-x, y, -z, w), glm::vec4(-1, 0, 0, 0) , glm::vec2(1, 1) };
	vertexData[22].position = glm::vec3(-x, y, -z);
	vertexData[22].normal = glm::vec3(-1, 0, 0);
	vertexData[22].texcoord = glm::vec2(1, 1);

	// vertex 7
	// face 5
	//vertexData[23] = { glm::vec4(-x, y, -z, w), glm::vec4(0, 1, 0, 0) , glm::vec2(0, 0) };
	vertexData[23].position = glm::vec3(-x, y, -z);
	vertexData[23].normal = glm::vec3(0, 1, 0);
	vertexData[23].texcoord = glm::vec2(0, 0);
	//
	//vertexData[23] = { glm::vec4(), glm::vec4() , glm::vec2() };
	//---------------------------------------------------------//


	unsigned int indexData[] =
	{
		 0,  9,  6,  0,  6,  3,
		10, 21, 18, 10, 18,  7,
		 1, 12, 22,  1, 22, 11,
		 4, 15, 13,  4, 13,  2,
		 8, 19, 16,  8, 16,  5,
		23, 14, 17, 23, 17, 20,
	};

	// Create render data
	RendererData::BufferData vBuffer(sizeof(Vertex) * 24, vertexData, GL_STATIC_DRAW);
	RendererData::BufferData iBuffer(sizeof(unsigned int) * 36, indexData, GL_STATIC_DRAW);

	void* normOffset = (void*)(sizeof(glm::vec3));
	void* uvOffset = (void*)(sizeof(glm::vec3) * 2);
	std::vector<RendererData::AttributeData> attribs;
	attribs.push_back(RendererData::AttributeData(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0));
	attribs.push_back(RendererData::AttributeData(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), normOffset));
	attribs.push_back(RendererData::AttributeData(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), uvOffset));

	RendererData* rendererData = new RendererData();
	rendererData->Init(vBuffer, iBuffer, attribs, 36);

	AABB* collider = new AABB();
	std::vector<glm::vec3> verts;
	for (auto vData : vertexData)
		verts.push_back(vData.position);

	collider->Fit(verts);

	return new Mesh(rendererData, collider);
}

std::string AssetLoader::GetResourceFolder()
{
	char buf[FILENAME_MAX];
	GetCurrentDirectory(FILENAME_MAX, buf);

	std::string path = buf;
	return path + "\\..\\res\\";
}

std::string AssetLoader::ReadFile(const char* file)
{std::string result;
	std::ifstream fs(file, std::ios::in);

	if (!fs.is_open())
	{
		std::cerr << "Could not read file " << file << ". File does not exist." << std::endl;
		return "";
	}

	std::string line = "";
	while (!fs.eof())
	{
		std::getline(fs, line);
		result.append(line + "\n");
	}

	fs.close();

	return result;
	
}

std::string AssetLoader::ReadFile(std::string file)
{
	return ReadFile(file.c_str());
}