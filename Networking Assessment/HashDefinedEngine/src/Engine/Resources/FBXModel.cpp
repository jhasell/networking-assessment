#include "Engine\Resources\FBXModel.h"

const char* const FBXModel::FBX_RELATIVE_PATH = "models\\fbx\\";
const char* const FBXModel::OBJ_RELATIVE_PATH = "models\\obj\\";

FBXModel::FBXModel(FBXFile* fbx, ICollider* collider)
	: Asset(AssetType::FBX_MODEL), m_collider(collider)
{
	m_hasBeenDestroyed = false;
	m_hasAnimations = false;

	m_fbxFile = fbx;
}

FBXModel::~FBXModel()
{
	Destroy();
}

void FBXModel::UpdateAnimation(float time, bool loop, float fps)
{
	if (m_skeleton != nullptr && m_animation != nullptr)
	{
		m_skeleton->evaluate(m_animation, time, loop, fps);

		for (unsigned int bone_index = 0; bone_index < m_skeleton->m_boneCount; ++bone_index)
			m_skeleton->m_nodes[bone_index]->updateGlobalTransform();

		m_skeleton->updateBones();
	}
}

//void FBXModel::Render(GLuint shaderID)
//{
//	if (m_fbxFile != nullptr)
//	{
//		for (unsigned int i = 0; i < m_fbxFile->getMeshCount(); ++i)
//		{
//			FBXMeshNode* mesh = m_fbxFile->getMeshByIndex(i);
//
//			RendererData* renData = (RendererData*)mesh->m_userData;
//
//			renData->Render(shaderID);
//		}
//	}
//}

void FBXModel::Destroy()
{
	if (!m_hasBeenDestroyed)
	{
		// Cleanup the vertex data attached to each mesh
		for (unsigned int i = 0; i < m_fbxFile->getMeshCount(); ++i)
		{
			FBXMeshNode* mesh = m_fbxFile->getMeshByIndex(i);

			RendererData* renData = (RendererData*)mesh->m_userData;
			renData->Destroy();

			delete renData;
		}

		delete m_fbxFile;
		m_fbxFile = nullptr;

		m_hasBeenDestroyed = true;
	}
}

FBXSkeleton* FBXModel::GetSkeleton(unsigned int index)
{
	return m_fbxFile->getSkeletonByIndex(index);
}

FBXSkeleton* FBXModel::GetSkeleton() const
{
	return m_skeleton;
}

FBXAnimation* FBXModel::GetAnimation(unsigned int index)
{
	return m_fbxFile->getAnimationByIndex(index);
}

FBXAnimation* FBXModel::GetAnimation(const char* name)
{
	return m_fbxFile->getAnimationByName(name);
}

FBXAnimation* FBXModel::GetAnimation() const
{
	return m_animation;
}

ICollider* FBXModel::GetCollider()
{
	return m_collider;
}

void FBXModel::SetSkeleton(FBXSkeleton* skeleton)
{
	m_skeleton = skeleton;
}

void FBXModel::SetAnimation(FBXAnimation* animation)
{
	m_animation = animation;
}