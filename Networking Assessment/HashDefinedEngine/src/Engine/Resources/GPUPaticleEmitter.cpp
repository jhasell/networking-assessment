#include "Engine\Resources\GPUParticleEmitter.h"
#include "Engine\Resources\AssetLoader.h"
#include <string>

GPUParticleEmitter::GPUParticleEmitter()
	:m_particles(nullptr), m_maxParticles(0),
	m_position(0, 0, 0), m_drawShader(0),
	m_updateShader(0), m_lastDrawTime(0)
{
	m_vao[0] = 0;
	m_vao[1] = 0;
	m_vbo[0] = 0;
	m_vbo[1] = 0;
}

GPUParticleEmitter::~GPUParticleEmitter()
{
	delete[] m_particles;

	glDeleteVertexArrays(2, m_vao);
	glDeleteBuffers(2, m_vbo);

	glDeleteProgram(m_drawShader);
	glDeleteProgram(m_updateShader);
}

void GPUParticleEmitter::Init(unsigned int maxParticles,
	float lifespanMin, float lifespanMax, float velMin, 
	float velMax, float startSize, float endSize,
	const glm::vec4& startColour, const glm::vec4& endColour)
{
	m_startColour = startColour;
	m_endColour = endColour;
	m_startSize = startSize;
	m_endSize = endSize;
	m_velocityMin = velMin;
	m_velocityMax = velMax;
	m_lifespanMin = lifespanMin;
	m_lifespanMax = lifespanMax;
	m_maxParticles = maxParticles;

	m_particles = new GPUParticle[m_maxParticles];
	m_activeBuffer = 0;

	CreateBuffers();
	CreateUpdateShader();
	CreateDrawShader();
}

void GPUParticleEmitter::Render(float time, const glm::mat4& camera, 
	const glm::mat4& projView)
{
	// Update the particles using the transform feedback
	glUseProgram(m_updateShader);

	// Bind time information
	int location = glGetUniformLocation(m_updateShader, "time");
	glUniform1f(location, time);

	float deltaTime = time - m_lastDrawTime;
	m_lastDrawTime = time;

	location = glGetUniformLocation(m_updateShader, "deltaTime");
	glUniform1f(location, deltaTime);

	// Bind emitter's position
	location = glGetUniformLocation(m_updateShader, "emitterPosition");
	glUniform3fv(location, 1, &m_position[0]);

	// Disable rasterisation
	glEnable(GL_RASTERIZER_DISCARD);

	// Bind the buffer we will update
	glBindVertexArray(m_vao[m_activeBuffer]);

	// Work out the "other" buffer
	unsigned int otherBuffer = (m_activeBuffer + 1) % 2;

	// Bind the buffer we will update into as points
	// And begin transform feedback
	glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, m_vbo[otherBuffer]);
	glBeginTransformFeedback(GL_POINTS);
	glDrawArrays(GL_POINTS, 0, m_maxParticles);

	// Disable transform feedback and enable rasterization again
	glEndTransformFeedback();
	glDisable(GL_RASTERIZER_DISCARD);
	glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, 0);

	// Draw the particles using the Geometry Shader to billboard them
	glUseProgram(m_drawShader);

	location = glGetUniformLocation(m_drawShader, "projectionView");
	glUniformMatrix4fv(location, 1, false, &projView[0][0]);

	location = glGetUniformLocation(m_drawShader, "cameraTransform");
	glUniformMatrix4fv(location, 1, false, &camera[0][0]);

	// Draw particles in the "other" buffer
	glBindVertexArray(m_vao[otherBuffer]);
	glDrawArrays(GL_POINTS, 0, m_maxParticles);

	// Swap for the next frame
	m_activeBuffer = otherBuffer;
}

void GPUParticleEmitter::SetPosition(const glm::vec3& pos)
{
	m_position.x = pos.x;
	m_position.y = pos.y;
	m_position.z = pos.z;
}

void GPUParticleEmitter::CreateBuffers()
{
	// Create OpenGL buffers
	glGenVertexArrays(2, m_vao);
	glGenBuffers(2, m_vbo);

	// Setup first buffer
	glBindVertexArray(m_vao[0]);
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, m_maxParticles * sizeof(GPUParticle),
		m_particles, GL_STREAM_DRAW);

	glEnableVertexAttribArray(0); // position
	glEnableVertexAttribArray(1); // velocity
	glEnableVertexAttribArray(2); // lifetime
	glEnableVertexAttribArray(3); // lifespan
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(GPUParticle), 0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(GPUParticle), ((char*)0) + 12);
	glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, sizeof(GPUParticle), ((char*)0) + 24);
	glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE, sizeof(GPUParticle), ((char*)0) + 28);

	// Setup second buffer
	glBindVertexArray(m_vao[1]);
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo[1]);
	glBufferData(GL_ARRAY_BUFFER, m_maxParticles * sizeof(GPUParticle),
		0, GL_STREAM_DRAW);

	glEnableVertexAttribArray(0); // position
	glEnableVertexAttribArray(1); // velocity
	glEnableVertexAttribArray(2); // lifetime
	glEnableVertexAttribArray(3); // lifespan
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(GPUParticle), 0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(GPUParticle), ((char*)0) + 12);
	glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, sizeof(GPUParticle), ((char*)0) + 24);
	glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE, sizeof(GPUParticle), ((char*)0) + 28);

	// Clean up
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void GPUParticleEmitter::CreateUpdateShader()
{
	const char* vsPath = "GPUParticleUpdate.vert";
	unsigned int vs = LoadShader(GL_VERTEX_SHADER, vsPath);

	m_updateShader = glCreateProgram();
	glAttachShader(m_updateShader, vs);

	// Specify the data that we will steam back
	const char* varyings[] = { "position", "velocity", "lifetime", "lifespan" };
	glTransformFeedbackVaryings(m_updateShader, 4, varyings, GL_INTERLEAVED_ATTRIBS);

	// Link shader
	glLinkProgram(m_updateShader);

	// Verify shader
	int result = GL_FALSE;
	glGetProgramiv(m_updateShader, GL_LINK_STATUS, &result);
	if (result == GL_FALSE)
	{
		int infoLogLength = 0;
		glGetProgramiv(m_updateShader, GL_INFO_LOG_LENGTH, &infoLogLength);
		char* infoLog = new char[infoLogLength];

		glGetProgramInfoLog(m_updateShader, infoLogLength, 0, infoLog);
		printf("Error: Failed to link shader program!");
		printf("%s\n", infoLog);
		delete infoLog;
	}

	// Remove uneeded hangles
	glDeleteShader(vs);

	// Bind the shader so that we can set
	// some uniforms that don't change per-frame
	glUseProgram(m_updateShader);

	// Bind size information for interpolation that won't change
	int location = glGetUniformLocation(m_updateShader, "lifeMin");
	glUniform1f(location, m_lifespanMin);
	location = glGetUniformLocation(m_updateShader, "lifeMax");
	glUniform1f(location, m_lifespanMax);
}

void GPUParticleEmitter::CreateDrawShader()
{
	const char* vsPath = "GPUParticle.vert";
	const char* gsPath = "GPUParticle.geom";
	const char* fsPath = "GPUParticle.frag";

	unsigned int vs = LoadShader(GL_VERTEX_SHADER, vsPath);
	unsigned int gs = LoadShader(GL_GEOMETRY_SHADER, gsPath);
	unsigned int fs = LoadShader(GL_FRAGMENT_SHADER, fsPath);

	m_drawShader = glCreateProgram();
	glAttachShader(m_drawShader, vs);
	glAttachShader(m_drawShader, fs);
	glAttachShader(m_drawShader, gs);
	glLinkProgram(m_drawShader);

	// Verify shader
	int result = GL_FALSE;
	glGetProgramiv(m_drawShader, GL_LINK_STATUS, &result);
	if (result == GL_FALSE)
	{
		int infoLogLength = 0;
		glGetProgramiv(m_drawShader, GL_INFO_LOG_LENGTH, &infoLogLength);
		char* infoLog = new char[infoLogLength];

		glGetProgramInfoLog(m_drawShader, infoLogLength, 0, infoLog);
		printf("Error: Failed to link shader program!");
		printf("%s\n", infoLog);
		delete infoLog;
	}

	// Remove uneeded hangles
	glDeleteShader(vs);
	glDeleteShader(gs);
	glDeleteShader(fs);

	// Bind the shader so that we can set
	// some uniforms that don't change per-frame
	glUseProgram(m_drawShader);

	// Bind size information for interpolation that won't change
	int location = glGetUniformLocation(m_drawShader, "sizeStart");
	glUniform1f(location, m_startSize);
	location = glGetUniformLocation(m_drawShader, "sizeEnd");
	glUniform1f(location, m_endSize);

	// Bind colour information for interpolation that won't change
	location = glGetUniformLocation(m_drawShader, "colourStart");
	glUniform4fv(location, 1, &m_startColour[0]);
	location = glGetUniformLocation(m_drawShader, "colourEnd");
	glUniform4fv(location, 1, &m_endColour[0]);
}

unsigned int GPUParticleEmitter::LoadShader(unsigned int type, const char* path)
{
	//std::string result;
	//std::ifstream fs(path, std::ios::in);
	//
	//if (!fs.is_open())
	//{
	//	std::string str = "Could not read file " + std::string(path);
	//	str += ". File does not exist.";
	//	printf(str.c_str());
	//	return 0;
	//}
	//
	//std::string line = "";
	//while (!fs.eof())
	//{
	//	std::getline(fs, line);
	//	result.append(line + "\n");
	//}
	//
	//fs.close();

	std::string fullPath = AssetLoader::GetResourceFolder();

	switch (type)
	{
	case GL_VERTEX_SHADER:
		fullPath += "shaders\\vertex\\";
		break;

	case GL_GEOMETRY_SHADER:
		fullPath += "shaders\\geometry\\";
		break;

	case GL_FRAGMENT_SHADER:
		fullPath += "shaders\\fragment\\";
	}

	fullPath += std::string(path);

	

	std::string fileContents = AssetLoader::ReadFile(fullPath);
	const char* source = fileContents.c_str();
	
	unsigned int shader = glCreateShader(type);
	glShaderSource(shader, 1, &source, 0);
	glCompileShader(shader);

	return shader;
}