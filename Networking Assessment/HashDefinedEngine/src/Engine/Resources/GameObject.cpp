#include "Engine\Resources\GameObject.h"

GameObject::GameObject()
{
	m_collider = new ICollider(ICollider::ColliderType::NONE);
	m_mesh = nullptr;

	m_shader = nullptr;

	m_scale = glm::vec3(1);
	m_rotation = glm::vec3(0);
	m_position = glm::vec3(0);
}

GameObject::GameObject(Mesh* mesh, Shader* shader)
{
	m_collider = new ICollider(mesh->GetCollider()->GetType());

	m_hasBeenDestroyed = false;
	m_mesh = mesh;

	m_shader = shader;

	m_scale = glm::vec3(1);
	m_rotation = glm::vec3(0);
	m_position = glm::vec3(0);
}

GameObject::GameObject(FBXModel* fbx, Shader* shader)
{
	m_collider = new ICollider(fbx->GetCollider()->GetType());

	m_hasBeenDestroyed = false;
	m_fbx = fbx;

	m_shader = shader;

	m_scale = glm::vec3(1);
	m_rotation = glm::vec3(0);
	m_position = glm::vec3(0);
}

GameObject::~GameObject()
{
	Destroy();
}

void GameObject::Update(float deltaTime)
{

}

//void GameObject::Render()
//{
//	if (m_shader != nullptr)
//	{
//		if (m_mesh != nullptr)
//			m_mesh->Render(m_shader->GetProgramID());
//
//		if (m_fbx != nullptr)
//			m_fbx->Render(m_shader->GetProgramID());
//	}	
//}

void GameObject::Destroy()
{
	if (!m_hasBeenDestroyed)
	{
		if (m_fbx != nullptr)
		{
			m_fbx->Destroy();
			delete m_fbx;
			m_fbx = nullptr;
		}

		if (m_mesh != nullptr)
		{
			m_mesh->Destroy();
			delete m_mesh;
			m_mesh = nullptr;
		}

		m_hasBeenDestroyed = true;
	}
}

glm::mat4& GameObject::GetMVP(const glm::mat4& projectionView, glm::mat4& mvp)
{
	static glm::vec3 xAxis = glm::vec3(1, 0, 0);
	static glm::vec3 yAxis = glm::vec3(0, 1, 0);
	static glm::vec3 zAxis = glm::vec3(0, 0, 1);

	mvp = glm::mat4(projectionView);

	// Scale/Rotate/Translate object
	mvp = glm::scale(mvp, m_scale);
	mvp = glm::rotate(mvp, m_rotation.x, xAxis);
	mvp = glm::rotate(mvp, m_rotation.y, yAxis);
	mvp = glm::rotate(mvp, m_rotation.z, zAxis);
	mvp = glm::translate(mvp, m_position);

	return mvp;
}

void GameObject::SetScale(glm::vec3 scale)
{
	m_scale = scale;
}

void GameObject::SetScale(float scale)
{
	m_scale.x = m_scale.y = m_scale.z = scale;
}

void GameObject::SetRotation(glm::vec3 rotation)
{
	m_rotation = rotation;
}

void GameObject::SetPosition(glm::vec3 position)
{
	m_position = position;
	UpdateColliderPosition();
}

void GameObject::SetMesh(Mesh* mesh)
{
	if (mesh != nullptr)
		m_mesh = mesh;
}

void GameObject::SetFBX(FBXModel* fbx)
{
	if (fbx != nullptr)
		m_fbx = fbx;
}

void GameObject::SetShader(Shader* shader)
{
	m_shader = shader;
}

glm::vec3 GameObject::GetScale()
{
	return m_scale;
}

glm::vec3 GameObject::GetRotation()
{
	return m_rotation;
}

glm::vec3 GameObject::GetPosition()
{
	return m_position;
}

Mesh* GameObject::GetMesh()
{
	return m_mesh;
}

FBXModel* GameObject::GetFBX()
{
	return m_fbx;
}

Shader* GameObject::GetShader()
{
	return m_shader;
}

ICollider* GameObject::GetCollider()
{
	return m_collider;
}

void GameObject::UpdateColliderPosition()
{
	if (m_collider->GetType() == ICollider::ColliderType::AABB)
	{
		AABB* collider = (AABB*)m_collider;
		//collider-> = m_position;

	}
	else if (m_collider->GetType() == ICollider::ColliderType::SPHERE)
	{
		BoundingSphere* collider = (BoundingSphere*)m_collider;
		collider->m_center = m_position;
	}
}