#include "Engine\Resources\Shader.h"
#include "Engine\Resources\AssetLoader.h"


Shader::Shader(const char* vsFile, const char* fsFile, const char* gsFile, const char* tcsFile, const char* tesFile)
	: Asset(AssetType::SHADER)
{
	m_vertID = AssetLoader::LoadShader(GL_VERTEX_SHADER, vsFile);
	m_fragID = AssetLoader::LoadShader(GL_FRAGMENT_SHADER, fsFile);
	m_geomID = AssetLoader::LoadShader(GL_GEOMETRY_SHADER, gsFile);
	m_tessContID = AssetLoader::LoadShader(GL_TESS_CONTROL_SHADER, tcsFile);
	m_tessEvalID = AssetLoader::LoadShader(GL_TESS_EVALUATION_SHADER, tesFile);

	m_hasBeenDestroyed = false;
}

Shader::~Shader()
{
	if (!m_hasBeenDestroyed)
	{
		Stop();

		if (m_vertID > 0)
		{
			glDetachShader(m_programID, m_vertID);
			glDeleteShader(m_vertID);
		}

		if (m_fragID > 0)
		{
			glDetachShader(m_programID, m_fragID);
			glDeleteShader(m_fragID);
		}

		if (m_geomID > 0)
		{
			glDetachShader(m_programID, m_geomID);
			glDeleteShader(m_geomID);
		}

		if (m_tessContID > 0)
		{
			glDetachShader(m_programID, m_tessContID);
			glDeleteShader(m_tessContID);
		}

		if (m_tessEvalID > 0)
		{
			glDetachShader(m_programID, m_tessEvalID);
			glDeleteShader(m_tessEvalID);
		}

		glDeleteProgram(m_programID);

		m_hasBeenDestroyed = true;
	}
}

void Shader::Start()
{
	glUseProgram(m_programID);
}

void Shader::Stop()
{
	glUseProgram(0);
}

unsigned int Shader::GetProgramID()
{
	return m_programID;
}

void Shader::CreateProgram()
{
	// Create shader program ID
	m_programID = glCreateProgram();

	//Attach given shaders
	if (m_vertID > 0)
		glAttachShader(m_programID, m_vertID);

	if (m_fragID > 0)
		glAttachShader(m_programID, m_fragID);

	if (m_geomID > 0)
		glAttachShader(m_programID, m_geomID);

	if (m_tessContID > 0)
		glAttachShader(m_programID, m_tessContID);

	if (m_tessEvalID > 0)
		glAttachShader(m_programID, m_tessEvalID);

	// Bind attributes
	BindAttributes();

	// Link shaders into program
	glLinkProgram(m_programID);
	glValidateProgram(m_programID);

	// Check if link was successful
	int result = GL_FALSE;
	glGetProgramiv(m_programID, GL_LINK_STATUS, &result);
	if (result == GL_FALSE)
	{
		int infoLogLength = 0;
		glGetProgramiv(m_programID, GL_INFO_LOG_LENGTH, &infoLogLength);
		
		char* infoLog = new char[infoLogLength];

		glGetProgramInfoLog(m_programID, infoLogLength, 0, infoLog);
		printf("Error: Failed to link shader program!\n");
		printf("%s\n", infoLog);
		delete infoLog;
	}
	else
	{
		// Load all uniforms
		Start();
		GetAllUniformLocations();
		Stop();
	}
}

void Shader::BindAttribute(int attrLocation, const char* name)
{
	glBindAttribLocation(m_programID, attrLocation, name);
}

int Shader::GetUniformLocation(const char* uniformName)
{
	return glGetUniformLocation(this->GetProgramID(), uniformName);
}

void Shader::LoadFloat(int location, float value)
{
	glUniform1f(location, value);
}

void Shader::LoadVec2(int location, const glm::vec2& value)
{
	glUniform2f(location, value.x, value.y);
}

void Shader::LoadVec3(int location, const glm::vec3& value)
{
	glUniform3f(location, value.x, value.y, value.z);
}

void Shader::LoadVec4(int location, const glm::vec4& value)
{
	glUniform4f(location, value.r, value.g, value.b, value.a);
}

void Shader::LoadBoolean(int location, bool value)
{
	float toLoad = 0;
	if (value)
		toLoad = 1;

	glUniform1f(location, toLoad);
}

void Shader::LoadMat4(int location, const glm::mat4& value)
{
	glUniformMatrix4fv(location, 1, GL_FALSE, (float*)&value);
}

void Shader::LoadSampler2D(int uniformLocation, unsigned int textureSlot, unsigned int textureID)
{
	if (textureSlot < 0 || textureSlot > 31 || textureID < 0)
		return;

	GLenum actualSlot;
	switch (textureSlot)
	{
		case 0: actualSlot = GL_TEXTURE0; break;
		case 1: actualSlot = GL_TEXTURE1; break;
		case 2: actualSlot = GL_TEXTURE2; break;
		case 3: actualSlot = GL_TEXTURE3; break;
		case 4: actualSlot = GL_TEXTURE4; break;
		case 5: actualSlot = GL_TEXTURE5; break;
		case 6: actualSlot = GL_TEXTURE6; break;
		case 7: actualSlot = GL_TEXTURE7; break;
		case 8: actualSlot = GL_TEXTURE8; break;
		case 9: actualSlot = GL_TEXTURE9; break;
		case 10: actualSlot = GL_TEXTURE10; break;
		case 11: actualSlot = GL_TEXTURE11; break;
		case 12: actualSlot = GL_TEXTURE12; break;
		case 13: actualSlot = GL_TEXTURE13; break;
		case 14: actualSlot = GL_TEXTURE14; break;
		case 15: actualSlot = GL_TEXTURE15; break;
		case 16: actualSlot = GL_TEXTURE16; break;
		case 17: actualSlot = GL_TEXTURE17; break;
		case 18: actualSlot = GL_TEXTURE18; break;
		case 19: actualSlot = GL_TEXTURE19; break;
		case 20: actualSlot = GL_TEXTURE20; break;
		case 21: actualSlot = GL_TEXTURE21; break;
		case 22: actualSlot = GL_TEXTURE22; break;
		case 23: actualSlot = GL_TEXTURE23; break;
		case 24: actualSlot = GL_TEXTURE24; break;
		case 25: actualSlot = GL_TEXTURE25; break;
		case 26: actualSlot = GL_TEXTURE26; break;
		case 27: actualSlot = GL_TEXTURE27; break;
		case 28: actualSlot = GL_TEXTURE28; break;
		case 29: actualSlot = GL_TEXTURE29; break;
		case 30: actualSlot = GL_TEXTURE30; break;
		case 31: actualSlot = GL_TEXTURE31; break;
		default: return;
	}

	glActiveTexture(actualSlot);
	glBindTexture(GL_TEXTURE_2D, textureID);
	glUniform1i(uniformLocation, textureID);
}