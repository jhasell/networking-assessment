#include "Engine\Resources\Terrain.h"

#include "AIE\gl_core_4_4.h"
#include "glm\ext.hpp"
#include "Engine\Resources\AssetLoader.h"


Terrain::Terrain(Shader* shader, unsigned int rows, unsigned int cols, float spacing)
{
	m_shader = shader;

	m_mesh = nullptr;

	m_rows = rows;
	m_cols = cols;

	Generate(rows, cols, spacing);
}

Terrain::~Terrain()
{
	if (m_mesh != nullptr)
	{
		delete m_mesh;
		m_mesh = nullptr;
	}
}

bool Terrain::Generate(unsigned int rows, unsigned int cols, float spacing)
{
	m_rows = rows;
	m_cols = cols;
	bool result = false;

	float scale = (1.0f / (rows - 1)) * 3;

	float* perlinData = GeneratePerlin(scale, 3, 2.0f, 0.3f);
	m_mesh = AssetLoader::GenerateGrid(rows, cols, perlinData, spacing);

	return true;
}

float* Terrain::GeneratePerlin(float scale, unsigned int octaves, float amplitude, float persistance)
{
	if (m_rows <= 0 || m_cols <= 0)
		return nullptr;

	float* perlin_data = new float[m_rows * m_cols];

	for (unsigned int x = 0; x < m_rows; x++)
	{
		for (unsigned int y = 0; y < m_cols; y++)
		{
			unsigned int index = y * m_rows + x;

			float a = amplitude;
			float p = persistance;

			perlin_data[index] = 0;

			for (unsigned int o = 0; o < octaves; o++)
			{
				float freq = powf(2, (float)o);
				float perlin_sample = glm::perlin(glm::vec2((float)x, (float)y) * scale * freq) * 0.5f + 0.5f;

				perlin_data[index] += perlin_sample * a;
				a *= p;
			}
		}
	}

	return perlin_data;
}

//void Terrain::Render()
//{
//	if(m_mesh != nullptr && m_shader != nullptr)
//		m_mesh->Render(m_shader->GetProgramID());
//}

unsigned int Terrain::GetNumRows()
{
	return m_rows;
}

unsigned int Terrain::GetNumCols()
{
	return m_cols;
}

Mesh* Terrain::GetMesh()
{
	return m_mesh;
}

Shader* Terrain::GetShader()
{
	return m_shader;
}