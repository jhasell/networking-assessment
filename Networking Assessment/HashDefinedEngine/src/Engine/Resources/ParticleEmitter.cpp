#include "Engine\Resources\ParticleEmitter.h"

ParticleEmitter::ParticleEmitter(ParticleType type)
	: m_type(type),
	m_particles(nullptr),
	m_firstDead(0),
	m_maxParticles(0),
	m_position(0, 0, 0),
	m_vao(0), m_vbo(0), m_ibo(0),
	m_vertexData(nullptr)
{
}

ParticleEmitter::~ParticleEmitter()
{
	delete[] m_particles;
	delete[] m_vertexData;

	glDeleteVertexArrays(1, &m_vao);
	glDeleteBuffers(1, &m_vbo);
	glDeleteBuffers(1, &m_ibo);
}

void ParticleEmitter::Initialise(unsigned int maxParticles, unsigned int emitRate, float lifetimeMin,
	float lifetimeMax, float velocityMin, float velocityMax, float startSize, float endSize,
	const glm::vec4& startColour, const glm::vec4& endColour)
{
	// Setup emit timers;
	m_emitTimer = 0;
	m_emitRate = 1.0f / emitRate;

	// Store all variables passed in
	m_startColour = startColour;
	m_endColour = endColour;
	m_startsize = startSize;
	m_endsize = endSize;
	m_velocityMin = velocityMin;
	m_velocityMax = velocityMax;
	m_lifeSpanMin = lifetimeMin;
	m_lifeSpanMax = lifetimeMax;
	m_maxParticles = maxParticles;

	// Create particle array
	m_particles = new Particle[m_maxParticles];
	m_firstDead = 0;

	// Create the array of vertices for the particles
	// 4 vertices per particle for a quad
	// will be filled during update
	m_vertexData = new ParticleVertex[m_maxParticles * 4];

	// Create the index buffer data for the particles
	// 6 indices per quad of 2 triangles
	// fill it now as it never changes
	unsigned int* indexData = new unsigned int[m_maxParticles * 6];
	for (unsigned int i = 0; i < m_maxParticles; i++)
	{
		indexData[i * 6 + 0] = i * 4 + 0;
		indexData[i * 6 + 1] = i * 4 + 1;
		indexData[i * 6 + 2] = i * 4 + 2;

		indexData[i * 6 + 3] = i * 4 + 0;
		indexData[i * 6 + 4] = i * 4 + 2;
		indexData[i * 6 + 5] = i * 4 + 3;
	}

	// Create openGL buffers
	glGenVertexArrays(1, &m_vao);
	glBindVertexArray(m_vao);

	glGenBuffers(1, &m_vbo);
	glGenBuffers(1, &m_ibo);

	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glBufferData(GL_ARRAY_BUFFER, m_maxParticles * 4 * sizeof(ParticleVertex),
		m_vertexData, GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_maxParticles * 6 * sizeof(unsigned int),
		indexData, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0); // Position
	glEnableVertexAttribArray(1); // Colour
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(ParticleVertex), 0);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(ParticleVertex), ((char*)0) + 16);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	delete[] indexData;
}

void ParticleEmitter::Emit()
{
	// Only emit if there is a dead particle to use
	if (m_firstDead >= m_maxParticles)
		return;

	// Resurrect the first dead particle
	Particle& particle = m_particles[m_firstDead++];

	// Randomise its lifespan
	particle.lifetime = 0;
	particle.lifespan = (rand() / (float)RAND_MAX) *
		(m_lifeSpanMax - m_lifeSpanMin) + m_lifeSpanMin;

	// Set starting size and colour
	particle.colour = m_startColour;
	particle.size = m_startsize;

	//// Randomise velocity direction & strength
	//float velocity = (rand() / (float)RAND_MAX) *
	//	(m_velocityMax - m_velocityMin) + m_velocityMin;
	//particle.velocity.x = (rand() / (float)RAND_MAX) * 2 - 1;
	//particle.velocity.y = (rand() / (float)RAND_MAX) * 2 - 1;
	//particle.velocity.z = (rand() / (float)RAND_MAX) * 2 - 1;
	//particle.velocity = glm::normalize(particle.velocity) * velocity;

	// Get position based on type
	switch (m_type)
	{
	case ParticleType::POINT:
		EmitPoint(particle);
		break;
	case ParticleType::SPHERE:
		EmitSphere(particle);
		break;
	case ParticleType::RECTANGLE:
		EmitRectangle(particle);
		break;
	case ParticleType::PLANE:
		EmitPlane(particle);
		break;
	case ParticleType::LINE:
		EmitLine(particle);
		break;
	case ParticleType::SPHERE_SURFACE:
		EmitSphereSurface(particle);
		break;
	case ParticleType::RING:
		EmitRing(particle);
		break;
	}
}

void ParticleEmitter::EmitPoint(Particle& particle)
{
	// Randomise velocity direction & strength
	float velocity = (rand() / (float)RAND_MAX) *
		(m_velocityMax - m_velocityMin) + m_velocityMin;
	particle.velocity.x = (rand() / (float)RAND_MAX) * 2 - 1;
	particle.velocity.y = (rand() / (float)RAND_MAX) * 2 - 1;
	particle.velocity.z = (rand() / (float)RAND_MAX) * 2 - 1;
	particle.velocity = glm::normalize(particle.velocity) * velocity;

	// Assign it's starting position
	particle.position = m_position;
}

void ParticleEmitter::EmitSphere(Particle& particle)
{
	// Assign it's starting position
	//particle.position = m_position;




}

void ParticleEmitter::EmitRectangle(Particle& particle)
{
	// Assign it's starting position
	//particle.position = m_position;
}

void ParticleEmitter::EmitPlane(Particle& particle)
{
	static float planeX = 0.0f;
	static float planeY = 0.0f;
	static float planeWidth = 10.0f;
	static float planeHeight = 10.0f;

	// Assign it's starting position
	particle.position.x = (rand() / (float)RAND_MAX) *
		(planeWidth - planeX) + planeX;
	particle.position.z = (rand() / (float)RAND_MAX) *
		(planeHeight - planeY) + planeY;
	particle.position.y = 0;

	// Assign velocity
	float velocity = (rand() / (float)RAND_MAX) *
		(m_velocityMax - m_velocityMin) + m_velocityMin;
	particle.velocity = glm::vec3(0, 1, 0) * velocity;
}

void ParticleEmitter::EmitLine(Particle& particle)
{
	static float lineX = 0.0f;
	static float lineX2 = 10.0f;

	// Assign it's starting position
	particle.position.x = (rand() / (float)RAND_MAX) *
		(lineX2 - lineX) + lineX;
	particle.position.z = 0;
	particle.position.y = 0;

	// Assign velocity
	float velocity = (rand() / (float)RAND_MAX) *
		(m_velocityMax - m_velocityMin) + m_velocityMin;
	particle.velocity = glm::vec3(0, 1, 0) * velocity;
}

void ParticleEmitter::EmitSphereSurface(Particle& particle)
{
	// Assign it's starting position
	//particle.position = m_position;
}

void ParticleEmitter::EmitRing(Particle& particle)
{
	static float radius = 10.0f;
	static float angle = 0.0f;
	static int steps = 10;

	angle += glm::pi<float>() / steps;
	// Assign it's starting position
	particle.position.x = radius * glm::cos(angle);
	particle.position.z = radius * glm::sin(angle);
	particle.position.y = 0;

	// Assign velocity
	float velocity = (rand() / (float)RAND_MAX) *
		(m_velocityMax - m_velocityMin) + m_velocityMin;
	particle.velocity = glm::vec3(0, 1, 0) * velocity;
}

void ParticleEmitter::Update(float deltaTime, const glm::mat4& cameraTransform)
{
	using glm::vec3;
	using glm::vec4;

	// Spawn particles
	m_emitTimer += deltaTime;
	while (m_emitTimer > m_emitRate)
	{
		Emit();
		m_emitTimer -= m_emitRate;
	}

	unsigned int quad = 0;

	// Update particles and turn live particles into billboard quads
	for (unsigned int i = 0; i < m_firstDead; i++)
	{
		Particle* particle = &m_particles[i];

		particle->lifetime += deltaTime;
		if (particle->lifetime >= particle->lifespan)
		{
			// Swap last alive with this one
			*particle = m_particles[m_firstDead - 1];
			m_firstDead--;
		}
		else
		{
			// Move particle
			particle->position += particle->velocity * deltaTime;

			// Size particle
			particle->size = glm::mix(m_startsize, m_endsize,
				particle->lifetime / particle->lifespan);

			// Colour particle
			particle->colour = glm::mix(m_startColour, m_endColour,
				particle->lifetime / particle->lifespan);

			// Make a quad the correct size and colour
			float halfSize = particle->size * 0.5f;

			m_vertexData[quad * 4 + 0].position = vec4(halfSize, halfSize, 0, 1);
			m_vertexData[quad * 4 + 0].colour = particle->colour;

			m_vertexData[quad * 4 + 1].position = vec4(-halfSize, halfSize, 0, 1);
			m_vertexData[quad * 4 + 1].colour = particle->colour;

			m_vertexData[quad * 4 + 2].position = vec4(-halfSize, -halfSize, 0, 1);
			m_vertexData[quad * 4 + 2].colour = particle->colour;

			m_vertexData[quad * 4 + 3].position = vec4(halfSize, -halfSize, 0, 1);
			m_vertexData[quad * 4 + 3].colour = particle->colour;

			// Create billboard transform
			vec3 zAxis = glm::normalize(vec3(cameraTransform[3]) - particle->position);
			vec3 xAxis = glm::cross(vec3(cameraTransform[1]), zAxis);
			vec3 yAxis = glm::cross(zAxis, xAxis);
			glm::mat4 billboard(
				vec4(xAxis, 0),
				vec4(yAxis, 0),
				vec4(zAxis, 0),
				vec4(0, 0, 0, 1));

			m_vertexData[quad * 4 + 0].position = billboard *
				m_vertexData[quad * 4 + 0].position +
				vec4(particle->position, 0);

			m_vertexData[quad * 4 + 1].position = billboard *
				m_vertexData[quad * 4 + 1].position +
				vec4(particle->position, 0);

			m_vertexData[quad * 4 + 2].position = billboard *
				m_vertexData[quad * 4 + 2].position +
				vec4(particle->position, 0);

			m_vertexData[quad * 4 + 3].position = billboard *
				m_vertexData[quad * 4 + 3].position +
				vec4(particle->position, 0);

			++quad;
		}
	}
}

void ParticleEmitter::Draw()
{
	// Sync the particle vertex buffer
	// Based on how many alive particles there are
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glBufferSubData(GL_ARRAY_BUFFER, 0,
		m_firstDead * 4 * sizeof(ParticleVertex), m_vertexData);

	// Draw particles
	glBindVertexArray(m_vao);
	glDrawElements(GL_TRIANGLES, m_firstDead * 6, GL_UNSIGNED_INT, 0);
}