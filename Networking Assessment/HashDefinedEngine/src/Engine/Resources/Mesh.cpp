#include "Engine\Resources\Mesh.h"

Mesh::Mesh(RendererData* renData, ICollider* collider)
	: Asset(AssetType::MESH), m_collider(collider)
{
	if (renData != nullptr)
	{
		m_rendererData = renData;
		m_hasBeenDestroyed = false;
	}
	else
	{
		m_hasBeenDestroyed = true;
	}
}

Mesh::~Mesh()
{
	Destroy();
}

//void Mesh::Render(GLuint shaderID)
//{
//	if (m_rendererData != nullptr)
//		m_rendererData->Render(shaderID);
//}

void Mesh::Destroy()
{
	if (!m_hasBeenDestroyed && m_rendererData != nullptr)
	{
		m_rendererData->Destroy();
		delete m_rendererData;
		m_rendererData = nullptr;
		m_hasBeenDestroyed = true;
	}
}

ICollider* Mesh::GetCollider()
{
	return m_collider;
}

RendererData* Mesh::GetRenderData()
{
	return m_rendererData;
}