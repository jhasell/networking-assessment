#include "Engine\BaseApplication.h"

BaseApplication::BaseApplication(int width, int height, std::string title)
	: m_windowWidth(width), m_windowHeight(height), m_windowTitle(title)
{
	m_isGLFWInitialised = false;
	m_fps = false;
	m_showWireframe = false;
}

BaseApplication::~BaseApplication()
{
	if (m_isGLFWInitialised)
	{
		DeinitGLFW();
	}

	if (m_inputManager != nullptr)
	{
		delete m_inputManager;
		m_inputManager = nullptr;
	}
}

bool BaseApplication::Run()
{
	if (InitGLFW() == false)
		return false;

	m_inputManager = new InputManager(this);

	if (Startup() == false)
		return false;

	float previousTime = 0.0f;
	float previousFPSTime = 0.0f;
	float currentTime = 0.0f;
	float deltaTime = 0.0f;
	int fpsCounter = 0;

	m_isRunning = true;
	while (m_isRunning)
	{
		// Update application delta time
		currentTime = (float)glfwGetTime();
		deltaTime = currentTime - previousTime;
		previousTime = currentTime;

		// Update FPS
		fpsCounter++;
		if ((currentTime - previousFPSTime) > 1.0f)
		{
			m_fps = fpsCounter;
			fpsCounter = 0;
			previousFPSTime = currentTime;
		}

		// Call child functions
		Update(deltaTime);
		Render();

		// Render in GLFW
		glfwSwapBuffers(m_window);
		glfwPollEvents();
	}

	bool result = Shutdown();
	DeinitGLFW();

	return result;
}

GLFWwindow* BaseApplication::GetWindow() const
{
	return m_window;
}

int BaseApplication::GetWindowWidth() const
{
	//glfwGetWindowSize(m_window, &m_windowWidth, &m_windowHeight);
	return m_windowWidth;
}

int BaseApplication::GetWindowHeight() const
{
	//glfwGetWindowSize(m_window, &m_windowWidth, &m_windowHeight);
	return m_windowHeight;
}

InputManager* BaseApplication::GetInputManager() const
{
	return m_inputManager;
}

int BaseApplication::GetFPS()
{
	return m_fps;
}

void BaseApplication::ShowWireframe(bool show)
{
	if (show)
	{
		glPolygonMode(GL_FRONT, GL_LINE);
		m_showWireframe = true;
	}
	else
	{
		glPolygonMode(GL_FRONT, GL_FILL);
		m_showWireframe = false;
	}
}

bool BaseApplication::IsWireframeShowing()
{
	return m_showWireframe;
}

void BaseApplication::Stop()
{
	m_isRunning = false;
}

bool BaseApplication::InitGLFW()
{
	if (glfwInit() == false)
		return false;

	m_window = glfwCreateWindow(m_windowWidth, m_windowHeight, m_windowTitle.c_str(), nullptr, nullptr);

	if (m_window == nullptr)
	{
		glfwTerminate();
		return false;
	}

	glfwMakeContextCurrent(m_window);

	if (ogl_LoadFunctions() == ogl_LOAD_FAILED)
	{
		glfwDestroyWindow(m_window);
		glfwTerminate();
		return false;
	}

	// Move window to the center of the screen
	CenterGLFWWindow(m_window);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	m_isGLFWInitialised = true;
	return true;
}

void BaseApplication::DeinitGLFW()
{
	if(m_window != nullptr)
		glfwDestroyWindow(m_window);

	glfwTerminate();
	m_isGLFWInitialised = false;
}

void BaseApplication::CenterGLFWWindow(GLFWwindow* window)
{
	if (window == nullptr)
		return;

	int windowX, windowY;
	glfwGetWindowPos(window, &windowX, &windowY);

	int windowW, windowH;
	glfwGetWindowSize(window, &windowW, &windowH);

	int screenW, screenH;
	const GLFWvidmode* vidMode = glfwGetVideoMode(glfwGetPrimaryMonitor());
	screenW = vidMode->width;
	screenH = vidMode->height;

	int finalPosX = (screenW - windowW) / 2;
	int finalPosY = (screenH - windowH) / 2;
	glfwSetWindowPos(window, finalPosX, finalPosY);
}