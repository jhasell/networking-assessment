#include "Engine\Debugging\DebugGUI.h"

// -----------------------------------------------
// Callback Functions
// -----------------------------------------------
void OnMouseButton(GLFWwindow*, int b, int a, int m)
{
	TwEventMouseButtonGLFW(b, a);
}

void OnMousePosition(GLFWwindow*, double x, double y)
{
	TwEventMousePosGLFW((int)x, (int)y);
}

void OnMouseScroll(GLFWwindow*, double x, double y)
{
	TwEventMouseWheelGLFW((int)y);
}

void OnKey(GLFWwindow*, int k, int s, int a, int m)
{
	TwEventKeyGLFW(k, a);
}

void OnChar(GLFWwindow*, unsigned int c)
{
	TwEventCharGLFW(c, GLFW_PRESS);
}

void OnWindowResize(GLFWwindow*, int w, int h)
{
	TwWindowSize(w, h);
	glViewport(0, 0, w, h);
}

// -----------------------------------------------
// Class Functions
// -----------------------------------------------
DebugGUI::DebugGUI(const BaseApplication* app)
{
	m_hasBeenDestroyed = false;

	TwInit(TW_OPENGL_CORE, nullptr);
	TwWindowSize(app->GetWindowWidth(), app->GetWindowHeight());

	glfwSetMouseButtonCallback(app->GetWindow(), OnMouseButton);
	glfwSetCursorPosCallback(app->GetWindow(), OnMousePosition);
	glfwSetScrollCallback(app->GetWindow(), OnMouseScroll);
	glfwSetKeyCallback(app->GetWindow(), OnKey);
	glfwSetCharCallback(app->GetWindow(), OnChar);
	glfwSetWindowSizeCallback(app->GetWindow(), OnWindowResize);

	m_defaultBar = DebugGUI::CreateBar("default bar");
}

DebugGUI::~DebugGUI()
{
	Destroy();
}

void DebugGUI::AddRW(const char* name, TwType type, void* var, const char* properties)
{
	TwAddVarRW(m_defaultBar, name, type, var, properties);
}

void DebugGUI::AddRW(TwBar* bar, const char* name, TwType type, void* var, const char* properties)
{
	TwAddVarRW(bar, name, type, var, properties);
}

void DebugGUI::AddSeparator(const char* name, TwBar* bar, const char* properties)
{
	if (bar == nullptr)
		bar = m_defaultBar;

	TwAddSeparator(bar, name, properties);
}

TwBar* DebugGUI::CreateBar(const char* name)
{
	return TwNewBar(name);
}

TwBar* DebugGUI::GetDefaultBar()
{
	return m_defaultBar;
}

void DebugGUI::Render()
{
	TwDraw();
}

void DebugGUI::Destroy()
{
	if (!m_hasBeenDestroyed)
	{
		TwDeleteAllBars();
		TwTerminate();
		m_defaultBar = nullptr;
		m_hasBeenDestroyed = true;
	}
}