#include "Engine\Physics\Collider\BoundingSphere.h"

BoundingSphere::BoundingSphere()
	: ICollider(ColliderType::SPHERE), m_center(0), m_radius(0)
{
}

BoundingSphere::~BoundingSphere()
{
}

void BoundingSphere::Fit(const std::vector<glm::vec3>& points)
{
	glm::vec3 min(1e37f), max(-1e37f);

	for (auto& p : points)
	{
		if (p.x < min.x) min.x = p.x;
		if (p.y < min.y) min.y = p.y;
		if (p.z < min.z) min.z = p.z;

		if (p.x > max.x) max.x = p.x;
		if (p.y > max.y) max.y = p.y;
		if (p.z > max.z) max.z = p.z;
	}

	m_center = (min + max) * 0.5f;
	m_radius = glm::distance(min, m_center);
}

bool BoundingSphere::HasCollidedWithPlane(const glm::vec3 sphereCenter, float sphereRadius, glm::vec4& plane)
{
	//----------------------------------------------//
	// (d > sphereRadius)  -> in front of the plane //
	// (d < -sphereRadius) -> behind of the plane   //
	// else                -> on the plane          //
	//----------------------------------------------//

	float d = glm::dot(glm::vec3(plane), sphereCenter) + plane.w;
	
	if (d > sphereRadius)
		return false;
	else
		return true;
}