#include "Engine\Physics\Collider\AABB.h"

AABB::AABB()
	: ICollider(ColliderType::AABB)
{
	Reset();
}

AABB::~AABB()
{
}

void AABB::Reset()
{
	m_min = glm::vec3(1e37f);
	m_max = glm::vec3(-1e37f);
}

void AABB::Fit(const std::vector<glm::vec3>& points)
{
	for (auto& p : points)
	{
		if (p.x < m_min.x) m_min.x = p.x;
		if (p.y < m_min.y) m_min.y = p.y;
		if (p.z < m_min.z) m_min.z = p.z;
				  
		if (p.x > m_max.x) m_max.x = p.x;
		if (p.y > m_max.y) m_max.y = p.y;
		if (p.z > m_max.z) m_max.z = p.z;
	}
}

bool AABB::HasCollidedWithPlane(const glm::vec3 min, const glm::vec3 max, glm::vec4& plane)
{
	////----------------------------------//
	//// (d < 0) -> behind of the plane   //
	//// (d > 0) -> in front of the plane //
	//// else    -> on the plane          //
	////----------------------------------//
	//
	//float d = glm::dot(glm::vec3(plane), vertPos) + plane.w;
	//
	//if (d > 0)
	//	return false;
	//else
	//	return true;
	return false;
}