#include "Engine\Renderer\Renderer.h"

void Renderer::Prepare(const glm::vec4& clearColour)
{
	glClearColor(clearColour.r, clearColour.g, clearColour.b, clearColour.a);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Renderer::Render(RendererData* renData, Shader* shader)
{
	shader->Start();
	glBindVertexArray(renData->GetVAO());
	shader->LoadTextures();
	glDrawElements(GL_TRIANGLES, renData->GetIndexCount(), GL_UNSIGNED_INT, 0);
	shader->Stop();
}

void Renderer::Render(RendererData* renData)
{
	glBindVertexArray(renData->GetVAO());
	glDrawElements(GL_TRIANGLES, renData->GetIndexCount(), GL_UNSIGNED_INT, 0);
}