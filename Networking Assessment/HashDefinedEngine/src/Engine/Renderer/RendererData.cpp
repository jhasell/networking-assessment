#include "Engine\Renderer\RendererData.h"

RendererData::RendererData()
{
	m_vbo = 0;
	m_ibo = 0;
	m_vao = 0;
	
	m_indexCount = 0;

	m_hasBeenDestroyed = false;
}

RendererData::~RendererData()
{
	Destroy();
}

void RendererData::Init(BufferData& vbo, BufferData& ibo,
	std::vector<AttributeData>& attribArray, unsigned int indexCount)
{
	m_indexCount = indexCount;

	// Create & bind buffers
	glGenBuffers(1, &m_vbo);
	glGenBuffers(1, &m_ibo);
	glGenVertexArrays(1, &m_vao);
	glBindVertexArray(m_vao);

	// Create VBO
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glBufferData(GL_ARRAY_BUFFER, vbo.bufferSize, vbo.buffer, vbo.drawType);

	// Create IBO
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, ibo.bufferSize, ibo.buffer, ibo.drawType);

	if (attribArray.size() > 0)
	{
		for (unsigned int i = 0; i < attribArray.size(); i++)
		{
			glEnableVertexAttribArray(attribArray[i].m_index);
			glVertexAttribPointer(attribArray[i].m_index,
				attribArray[i].m_size, 
				attribArray[i].m_type, 
				attribArray[i].m_isNormalized, 
				attribArray[i].m_stride,
				attribArray[i].m_ptr);
		}
	}

	// Clean up
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

//void RendererData::Render(GLuint shaderID)
//{
//	if (m_vao != -1)
//	{
//		glUseProgram(shaderID);
//		glBindVertexArray(m_vao);
//		glDrawElements(GL_TRIANGLES, m_indexCount, GL_UNSIGNED_INT, 0);
//	}
//}

GLuint RendererData::GetVAO()
{
	return m_vao;
}

GLuint RendererData::GetIndexCount()
{
	return m_indexCount;
}

void RendererData::Destroy()
{
	if (!m_hasBeenDestroyed)
	{
		glDeleteVertexArrays(1, &m_vao);
		glDeleteBuffers(1, &m_vbo);
		glDeleteBuffers(1, &m_ibo);
		
		m_hasBeenDestroyed = true;
	}
}