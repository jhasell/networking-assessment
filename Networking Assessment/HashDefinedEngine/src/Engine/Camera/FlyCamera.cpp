#include "Engine\Camera\FlyCamera.h"

FlyCamera::FlyCamera()
{
	m_moveSpeed = 10.0f;
	m_rotateSpeed = 0.1f;
}

void FlyCamera::Update(float dt)
{
	glm::mat4 world = GetWorldTransform();
	GLFWwindow* window = glfwGetCurrentContext();

	// Get keyboard/mouse inputs
	HandleKeyboardInput(window, dt);
	HandleMouseInput(window, dt);
}

void FlyCamera::Move(EMoveType movement, float dt)
{
	glm::vec3 direction(0);
	glm::vec3 rotation(0);
	glm::vec3 worldUp(0, 1, 0);
	float rotationMultiplier = 5.0f;

	// Get old camera x/y/z axis & position
	glm::mat4 world = GetWorldTransform();
	glm::vec3 currXAxis = glm::vec3(world[0]);
	glm::vec3 currZAxis = glm::vec3(world[2]);
	glm::vec3 currPos = glm::vec3(world[3]);

	// Get new camera position direction/rotation
	switch (movement)
	{
		case EMoveType::UP:			direction += worldUp;	break;
		case EMoveType::DOWN:		direction -= worldUp;	break;
		case EMoveType::LEFT:		direction -= currXAxis; break;
		case EMoveType::RIGHT:		direction += currXAxis; break;
		case EMoveType::FORWARD:	direction -= currZAxis; break;
		case EMoveType::BACKWARD:	direction += currZAxis; break;

		case EMoveType::PITCH_UP:	rotation.x += rotationMultiplier; break;
		case EMoveType::PITCH_DOWN: rotation.x -= rotationMultiplier; break;
		case EMoveType::YAW_LEFT:	rotation.y += rotationMultiplier; break;
		case EMoveType::YAW_RIGHT:	rotation.y -= rotationMultiplier; break;
		case EMoveType::ROLL_LEFT:	rotation.z += rotationMultiplier; break;
		case EMoveType::ROLL_RIGHT: rotation.z -= rotationMultiplier; break;
	}

	// Get the new camera position/rotation
	// --- if direction > 0 then camera is moving
	// --- if direction = 0 then camera is rotating
	float dirLength = glm::length(direction);
	if (dirLength > 0.0f)
	{
		// Get camera position
		direction = ((float)dt * m_moveSpeed) * glm::normalize(direction);
		currPos += direction;
	}
	else
	{
		// Get camera rotation
		if (rotation.x != 0.0f)
			world = glm::rotate(world, m_rotateSpeed * dt * rotation.x, glm::vec3(1, 0, 0));

		if (rotation.y != 0.0f)
			world = glm::rotate(world, m_rotateSpeed * dt * rotation.y, glm::vec3(0, 1, 0));

		if(rotation.z != 0.0f)
			world = glm::rotate(world, m_rotateSpeed * dt * rotation.z, glm::vec3(0, 0, 1));

		currXAxis = glm::vec3(world[0]);
		currZAxis = glm::vec3(world[2]);
	}
	
	// Create the new camera matrix
	glm::mat4 newTransform;
	glm::vec3 xAxis = glm::normalize(glm::cross(worldUp, currZAxis));
	glm::vec3 yAxis = glm::normalize(glm::cross(currZAxis, xAxis));
	glm::vec3 zAxis = glm::normalize(currZAxis);

	newTransform[0] = glm::vec4(xAxis, 0);
	newTransform[1] = glm::vec4(yAxis, 0);
	newTransform[2] = glm::vec4(zAxis, 0);
	newTransform[3] = glm::vec4(currPos, 1);

	SetWorldTransform(newTransform);
}

void FlyCamera::SetMoveSpeed(float speed)
{
	m_moveSpeed = speed;
}

void FlyCamera::SetRotateSpeed(float speed)
{
	m_rotateSpeed = speed;
}

void FlyCamera::HandleKeyboardInput(GLFWwindow* window, float dt)
{
	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
		Move(EMoveType::FORWARD, dt);

	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
		Move(EMoveType::BACKWARD, dt);

	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
		Move(EMoveType::LEFT, dt);

	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
		Move(EMoveType::RIGHT, dt);

	if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS)
		Move(EMoveType::UP, dt);
	
	if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS)
		Move(EMoveType::DOWN, dt);


	// Get new camera rotation
	glm::vec2 rotation(0);

	if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
		Move(EMoveType::PITCH_UP, dt);

	if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
		Move(EMoveType::PITCH_DOWN, dt);

	if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
		Move(EMoveType::YAW_LEFT, dt);

	if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
		Move(EMoveType::YAW_RIGHT, dt);

	if (glfwGetKey(window, GLFW_KEY_RIGHT_CONTROL) == GLFW_PRESS)
		Move(EMoveType::ROLL_LEFT, dt);

	if (glfwGetKey(window, GLFW_KEY_KP_0) == GLFW_PRESS)
		Move(EMoveType::ROLL_RIGHT, dt);
}

void FlyCamera::HandleMouseInput(GLFWwindow* window, float dt)
{
	static bool viewBtnClicked = false;
	static double m_cursorX = 0.0;
	static double m_cursorY = 0.0;

	if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_2) == GLFW_PRESS)
	{
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
		int width, height;
		glfwGetFramebufferSize(window, &width, &height);

		if (viewBtnClicked == false)
		{
			m_cursorX = width / 2.0;
			m_cursorY = height / 2.0;

			glfwSetCursorPos(window, width / 2, height / 2);

			viewBtnClicked = true;
		}
		else
		{
			double mouseX, mouseY;
			glfwGetCursorPos(window, &mouseX, &mouseY);

			float xOffset = (float)(mouseX - m_cursorX);
			float yOffset = (float)(mouseY - m_cursorY);

			CalculateRotation(dt, xOffset, yOffset);
		}

		glfwSetCursorPos(window, width / 2, height / 2);
	}
	else
	{
		viewBtnClicked = false;
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	}
}

void FlyCamera::CalculateRotation(float dt, float xOffset, float yOffset)
{
	if (xOffset == 0.0f && yOffset == 0.0f)
		return;

	if (yOffset != 0.0f)
	{
		float rotation = (float)(m_rotateSpeed * dt * -yOffset);
		glm::mat4 rot = glm::rotate(rotation, glm::vec3(1, 0, 0));
		SetWorldTransform(GetWorldTransform() * rot);
	}

	if (xOffset != 0.0f)
	{
		float rotation = (float)(m_rotateSpeed * dt * -xOffset);
		glm::mat4 rot = glm::rotate(rotation, glm::vec3(0, 1, 0));
		SetWorldTransform(GetWorldTransform() * rot);
	}

	// Clean up rotation
	glm::mat4 oldWorld = GetWorldTransform();
	glm::mat4 newWorld;
	glm::vec3 worldUp = glm::vec3(0, 1, 0);

	// Right
	glm::vec3 oldForward = glm::vec3(oldWorld[2].x, oldWorld[2].y, oldWorld[2].z);

	newWorld[0] = glm::normalize(glm::vec4(glm::cross(worldUp, oldForward), 0));

	// Up
	glm::vec3 newRight = glm::vec3(newWorld[0].x, newWorld[0].y, newWorld[0].z);
	newWorld[1] = glm::normalize(glm::vec4(glm::cross(oldForward, newRight), 0));

	// Forward
	newWorld[2] = glm::normalize(oldWorld[2]);

	// Position
	newWorld[3] = oldWorld[3];

	SetView(glm::inverse(newWorld));
}