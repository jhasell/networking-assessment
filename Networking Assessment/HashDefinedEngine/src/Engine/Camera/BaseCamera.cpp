#include "Engine\Camera\BaseCamera.h"

BaseCamera::BaseCamera()
{
}

BaseCamera::~BaseCamera()
{
}

void BaseCamera::SetPerspective(float fov, float aspect, float near, float far)
{
	m_projectionTransform = glm::perspective(fov, aspect, near, far);
	UpdateProjectionViewTransform();
}

void BaseCamera::SetLookAt(glm::vec3 from, glm::vec3 to)
{
	m_viewTransform = glm::lookAt(from, to, glm::vec3(0, 1, 0));
	m_worldTransform = glm::inverse(m_viewTransform);
	UpdateProjectionViewTransform();
}

void BaseCamera::SetPosition(glm::vec3 pos)
{
	m_worldTransform[3] = glm::vec4(pos, 1);
	m_viewTransform = glm::inverse(m_worldTransform);
	UpdateProjectionViewTransform();
}

void BaseCamera::SetView(glm::mat4& view)
{
	m_viewTransform = view;
	m_worldTransform = glm::inverse(m_viewTransform);
	UpdateProjectionViewTransform();
}

void BaseCamera::SetWorldTransform(glm::mat4& world)
{
	m_worldTransform = world;
	m_viewTransform = glm::inverse(m_worldTransform);
	UpdateProjectionViewTransform();
}

glm::vec3 BaseCamera::GetPosition()
{
	return glm::vec3(m_worldTransform[3].x, m_worldTransform[3].y, m_worldTransform[3].z);
}

glm::mat4& BaseCamera::GetWorldTransform()
{
	return m_worldTransform;
}

glm::mat4& BaseCamera::GetViewTransform()
{
	return m_viewTransform;
}

glm::mat4& BaseCamera::GetProjectionTransform()
{
	return m_projectionTransform;
}

glm::mat4& BaseCamera::GetProjectionViewTransform()
{
	return m_projectionViewTransform;
}

glm::vec4* BaseCamera::GetFrustrumPlanes()
{
	glm::vec4* planes = new glm::vec4[6];
	glm::mat4 transform = GetProjectionViewTransform();

	// Right side
	planes[0] = glm::vec4(
		transform[0][3] - transform[1][0],
		transform[1][3] - transform[1][0],
		transform[2][3] - transform[2][0],
		transform[3][3] - transform[3][0]);

	// Left side
	planes[1] = glm::vec4(
		transform[0][3] + transform[0][0],
		transform[1][3] + transform[1][0],
		transform[2][3] + transform[2][0],
		transform[3][3] + transform[3][0]);

	// Top side
	planes[2] = glm::vec4(
		transform[0][3] - transform[0][1],
		transform[1][3] - transform[1][1],
		transform[2][3] - transform[2][1],
		transform[3][3] - transform[3][1]);

	// Bottom side
	planes[3] = glm::vec4(
		transform[0][3] + transform[0][1],
		transform[1][3] + transform[1][1],
		transform[2][3] + transform[2][1],
		transform[3][3] + transform[3][1]);

	// Far side
	planes[4] = glm::vec4(
		transform[0][3] - transform[0][2],
		transform[1][3] - transform[1][2],
		transform[2][3] - transform[2][2],
		transform[3][3] - transform[3][2]);

	// Near side
	planes[5] = glm::vec4(
		transform[0][3] + transform[0][2],
		transform[1][3] + transform[1][2],
		transform[2][3] + transform[2][2],
		transform[3][3] + transform[3][2]);

	for (unsigned int i = 0; i < 6; i++)
		planes[i] = glm::normalize(planes[i]);

	return planes;
}

void BaseCamera::UpdateProjectionViewTransform()
{
	m_projectionViewTransform = m_projectionTransform * m_viewTransform;
}
