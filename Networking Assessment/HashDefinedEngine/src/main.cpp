#include "Game\LobbyApplication.h"
#include "Game\MyApplication.h"

int main()
{
	unsigned int clientID = 0;
	RakNet::RakPeerInterface* peerInterface = nullptr;

	LobbyApplication* lobby = new LobbyApplication(
		400, 600, "Networking Assessment - Lobby");
	lobby->Run();
	
	clientID = lobby->GetClientID();
	peerInterface = lobby->GetPeerInterface();

	delete lobby;


	std::cout << "--------------------------------" << std::endl;
	std::cout << "--------------------------------" << std::endl;
	std::cout << "Lobby closed." << std::endl;
	std::cout << "Client ID: " << clientID << std::endl;
	std::cout << "Starting Game" << std::endl;
	std::cout << "--------------------------------" << std::endl;
	std::cout << "--------------------------------" << std::endl;

	//BaseApplication* game = new MyApplication(
	//	1280, 900, "Networking Assessment - Client");
	//game->Run();
	//delete game;

	// Stop console window from closing automatically.
	// Allows user to see error.
	system("pause");

	return 0;
} 