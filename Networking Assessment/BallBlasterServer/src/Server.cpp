#include "Server.h"

Server::Server()
{
	m_peerInterface = RakNet::RakPeerInterface::GetInstance();
	//m_peerInterface->ApplyNetworkSimulator(0.0f, 150, 20);
	m_connectionCounter = 1;
	m_serverGameObjectCounter = 1;
}

Server::~Server()
{
}

void Server::Run()
{
	std::cout << "Attempting to start server network interface." << std::endl;
	
	RakNet::SocketDescriptor sd(PORT, 0);
	RakNet::StartupResult result = m_peerInterface->Startup(MAX_CLIENTS, &sd, 1);
	
	if (result == RakNet::StartupResult::RAKNET_STARTED)
	{
		std::cout << "Server network interface started successfully.";
		m_peerInterface->SetMaximumIncomingConnections(MAX_CLIENTS);
		ProcessNetworkMessages();	// Main loop
	}
	else
	{
		std::cout << "Failed to start server." << std::endl;
		char* errorTag = "";
		
		switch (result)
		{
		case RakNet::StartupResult::COULD_NOT_GENERATE_GUID:
			errorTag = "COULD_NOT_GENERATE_GUID"; break;
		case RakNet::StartupResult::FAILED_TO_CREATE_NETWORK_THREAD:
			errorTag = "FAILED_TO_CREATE_NETWORK_THREAD"; break;
		case RakNet::StartupResult::INVALID_MAX_CONNECTIONS:
			errorTag = "INVALID_MAX_CONNECTIONS"; break;
		case RakNet::StartupResult::INVALID_SOCKET_DESCRIPTORS:
			errorTag = "INVALID_SOCKET_DESCRIPTORS"; break;
		case RakNet::StartupResult::PORT_CANNOT_BE_ZERO:
			errorTag = "PORT_CANNOT_BE_ZERO"; break;
		case RakNet::StartupResult::RAKNET_ALREADY_STARTED:
			errorTag = "RAKNET_ALREADY_STARTED"; break;
		case RakNet::StartupResult::SOCKET_FAILED_TEST_SEND:
			errorTag = "SOCKET_FAILED_TEST_SEND"; break;
		case RakNet::StartupResult::SOCKET_FAILED_TO_BIND:
			errorTag = "SOCKET_FAILED_TO_BIND"; break;
		case RakNet::StartupResult::SOCKET_FAMILY_NOT_SUPPORTED:
			errorTag = "SOCKET_FAMILY_NOT_SUPPORTED"; break;
		case RakNet::StartupResult::SOCKET_PORT_ALREADY_IN_USE:
			errorTag = "SOCKET_PORT_ALREADY_IN_USE"; break;
		case RakNet::StartupResult::STARTUP_OTHER_FAILURE:
			errorTag = "STARTUP_OTHER_FAILURE"; break;
		}

		std::cout << std::endl;
		std::cout << "--------------------------------------------------------" << std::endl;
		std::cout << "StartupResult Error: " << errorTag << std::endl;
		std::cout << "--------------------------------------------------------" << std::endl;
		std::cout << "PORT: " << PORT << std::endl;
		std::cout << "sd.blockingSocket: " << sd.blockingSocket << std::endl;
		std::cout << "sd.chromeInstance: " << sd.chromeInstance << std::endl;
		std::cout << "sd.extraSocketOptions: " << sd.extraSocketOptions << std::endl;
		std::cout << "sd.hostAddress: " << sd.hostAddress << std::endl;
		std::cout << "sd.port: " << sd.port << std::endl;
		std::cout << "sd.remotePortRakNetWasStartedOn_PS3_PSP2: " << sd.remotePortRakNetWasStartedOn_PS3_PSP2 << std::endl;
		std::cout << "sd.socketFamily: " << sd.socketFamily << std::endl;
		std::cout << "--------------------------------------------------------" << std::endl;
		std::cout << std::endl;

		system("pause"); // Stop console window from closing automatically. Allows user to see error.
	}
}

void Server::ProcessNetworkMessages()
{
	bool runServer = true;
	RakNet::Packet* packet = nullptr;

	while (runServer)
	{
		for (packet = m_peerInterface->Receive(); packet; m_peerInterface->DeallocatePacket(packet), packet = m_peerInterface->Receive())
		{
			switch (packet->data[0])
			{
				case ID_NEW_INCOMING_CONNECTION:
				{
					std::cout << "A connection is incoming.\n";
					AddNewConnection(packet->systemAddress);
					break;
				}
				case ID_DISCONNECTION_NOTIFICATION:
				{
					std::cout << "A client has disconnected.\n";
					RemoveConnection(packet->systemAddress);
					break;
				}
				case ID_CONNECTION_LOST:
				{
					std::cout << "A client lost the connection.\n";
					RemoveConnection(packet->systemAddress);
					break;
				}
				case ID_CLIENT_GAME_POSITION:
				{
					RakNet::BitStream bs(packet->data, packet->length, false);
					bs.IgnoreBytes(sizeof(RakNet::MessageID));
					UpdatePlayerPos(bs, packet->systemAddress);
					break;
				}
				case ID_CLIENT_GAME_SHOOT:
				{
					RakNet::BitStream bs(packet->data, packet->length, false);
					bs.IgnoreBytes(sizeof(RakNet::MessageID));
					CreateNewBullet(bs, packet->systemAddress);
					break;
				}
				default:
				{
					std::cout << "Received a message with a unknown id: " << packet->data[0];
					break;
				}
			}
		}
	}
}

void Server::AddNewConnection(RakNet::SystemAddress address)
{
	ConnectionInfo info;
	info.sysAddress = address;
	info.connectionID = m_connectionCounter++;

	m_connectedClients[info.connectionID] = info;

	SendClientIDToClient(info.connectionID);
	SendAllGameObjectsToClient(info.connectionID);
}

void Server::RemoveConnection(RakNet::SystemAddress address)
{
	for (auto it = m_connectedClients.begin(); it != m_connectedClients.end(); it++)
	{
		if (it->second.sysAddress == address)
		{
			m_connectedClients.erase(it);
			break;
		}
	}
}

unsigned int Server::GetClientIDFromAddress(RakNet::SystemAddress& address)
{
	for (auto i : m_connectedClients)
	{
		if (i.second.sysAddress == address)
			return i.first;
	}

	return 0;
}

void Server::SendClientIDToClient(unsigned int clientID)
{
	RakNet::BitStream bs;
	bs.Write((RakNet::MessageID)GameMessages::ID_SERVER_GAME_CLIENT_ID);
	bs.Write(clientID);
	
	m_gameObjects.push_back(new ServerGameObject(m_serverGameObjectCounter++));
	AddGameObjectToBitStream(bs, m_gameObjects.back());
	
	m_peerInterface->Send(&bs, IMMEDIATE_PRIORITY, RELIABLE_ORDERED,
		0, m_connectedClients[clientID].sysAddress, false);
}

void Server::AddGameObjectToBitStream(RakNet::BitStream& bs, ServerGameObject* gameObject)
{
	bs.Write(gameObject->id);
	bs.Write(gameObject->posX);
	bs.Write(gameObject->posY);
	bs.Write(gameObject->posZ);
	bs.Write(gameObject->velX);
	bs.Write(gameObject->velY);
	bs.Write(gameObject->velZ);
}

void Server::SendGameObjectToAllClients(ServerGameObject* gameObject, unsigned int clientIDToIgnore)
{
	//RakNet::BitStream bs;
	
	//bs.Write((RakNet::MessageID)GameMessages::ID_SERVER_CREATE_PLAYER);
	//bs.Write(gameObject.uiOwnerClientID);
	//bs.Write(gameObject.uiObjectID);
	
	//m_peerInterface->Send(&bs, HIGH_PRIORITY, RELIABLE_ORDERED, 0,
	//	m_connectedClients[clientIDToIgnore].sysAddress, true);
}

void Server::SendAllGameObjectsToClient(unsigned int clientID)
{
	
}

void Server::CreateNewBullet(RakNet::BitStream& bs, RakNet::SystemAddress& ownerSysAddress)
{

}

void Server::UpdatePlayerPos(RakNet::BitStream& bs, RakNet::SystemAddress& ownerSysAddress)
{

}