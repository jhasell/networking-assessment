#pragma once

#include "Raknet\MessageIdentifiers.h"
#include "Raknet\RakPeerInterface.h"
#include "Raknet\BitStream.h"

enum GameMessages
{
	ID_SERVER_LOBBY_CLIENT_ID = ID_USER_PACKET_ENUM + 1,
	ID_SERVER_NEW_CLIENT = ID_USER_PACKET_ENUM + 2,
	ID_SERVER_REMOVE_CLIENT = ID_USER_PACKET_ENUM + 3,
	ID_SERVER_LOBBY_CLIENT_READY = ID_USER_PACKET_ENUM + 4,
	ID_SERVER_START_GAME = ID_USER_PACKET_ENUM + 5,

	ID_CLIENT_LOBBY_READY = ID_USER_PACKET_ENUM + 6,


	ID_SERVER_GAME_CLIENT_ID = ID_USER_PACKET_ENUM + 7,
	ID_SERVER_GAME_CREATE_PLAYER = ID_USER_PACKET_ENUM + 8,
	ID_SERVER_GAME_CREATE_BULLET = ID_USER_PACKET_ENUM + 9,
	ID_SERVER_GAME_UPDATE_PLAYER_POSITION = ID_USER_PACKET_ENUM + 10,
	ID_SERVER_GAME_UPDATE_PLAYER_VELOCITY = ID_USER_PACKET_ENUM + 11,

	ID_CLIENT_GAME_POSITION = ID_USER_PACKET_ENUM + 12,
	ID_CLIENT_GAME_SHOOT = ID_USER_PACKET_ENUM + 13,
};

struct ServerGameObject
{
	unsigned int id;
	float posX, posY, posZ;
	float velX, velY, velZ;

	ServerGameObject(unsigned int gameObjectID)
	{
		posX = posY = posZ = 0;
		velX = velY = velZ = 0;
		id = gameObjectID;
	}
};

struct ConnectionInfo
{
	unsigned int connectionID;
	RakNet::SystemAddress sysAddress;
};

struct LobbyData
{
	unsigned int clientID;
	unsigned int mapRequestedNum;
	bool isReady;

	LobbyData(unsigned int id)
	{
		clientID = id;
		mapRequestedNum = 0;
		isReady = false;
	}
};