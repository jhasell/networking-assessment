#pragma once

#include <iostream>
#include <string>
#include <unordered_map>

#include "NetworkMessages.h"

class Server
{
public:
	Server();
	~Server();

	void Run();

private:

	void ProcessNetworkMessages();

	void AddNewConnection(RakNet::SystemAddress address);
	void RemoveConnection(RakNet::SystemAddress address);

	unsigned int GetClientIDFromAddress(RakNet::SystemAddress& address);
	void AddGameObjectToBitStream(RakNet::BitStream& bs, ServerGameObject* gameObject);

	void SendClientIDToClient(unsigned int clientID);
	void SendGameObjectToAllClients(ServerGameObject* gameObject, unsigned int clientIDToIgnore);
	void SendAllGameObjectsToClient(unsigned int clientID);

	void CreateNewBullet(RakNet::BitStream& bs, RakNet::SystemAddress& ownerSysAddress);
	void UpdatePlayerPos(RakNet::BitStream& bs, RakNet::SystemAddress& ownerSysAddress);

	// --------------------------------------------------------------------- //

	const unsigned short PORT = 13579;
	const unsigned int MAX_CLIENTS = 32;
	RakNet::RakPeerInterface* m_peerInterface;

	unsigned int m_connectionCounter;
	unsigned int m_serverGameObjectCounter;
	std::unordered_map<unsigned int, ConnectionInfo> m_connectedClients;
	std::vector<ServerGameObject*> m_gameObjects;
};